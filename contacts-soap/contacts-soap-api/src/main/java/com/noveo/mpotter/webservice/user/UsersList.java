/*
* Created with IntelliJ IDEA.
* User: mpotter
* Date: 20.01.14
* Time: 15:04
*/
package com.noveo.mpotter.webservice.user;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * Contact users list class.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "users" })
@XmlRootElement(name = "usersList")
public class UsersList {

    @XmlElement(nillable = true)
    protected List<User> users;

    public List<User> getUsers() {
        if (users == null) {
            users = new ArrayList<User>();
        }
        return this.users;
    }

}
