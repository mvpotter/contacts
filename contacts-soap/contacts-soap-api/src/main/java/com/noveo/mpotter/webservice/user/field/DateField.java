/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 05.08.13
 * Time: 18:29
 */
package com.noveo.mpotter.webservice.user.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * Date field.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "date", propOrder = { "id", "name", "date" })
public class DateField extends AbstractField {

    private Date date;

    public DateField() {
    }

    public DateField(final String name, final Date date) {
        super(name);
        setDate(date);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

}
