/*
* Created with IntelliJ IDEA.
* User: mpotter
* Date: 18.12.13
* Time: 18:16
*/
package com.noveo.mpotter.webservice.user;

import com.noveo.mpotter.webservice.common.Page;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * User web service.
 */
@WebService(targetNamespace = "http://mpotter.noveo.com/webservice/user", name = "UserClient")
@XmlSeeAlso({ com.noveo.mpotter.webservice.user.User.class,
              com.noveo.mpotter.webservice.user.UsersList.class })
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface UserClient {

    @WebMethod
    @WebResult(name = "usersList", targetNamespace = "http://mpotter.noveo.com/webservice/user",
               partName = "usersList")
    UsersList getUsersList(@WebParam(partName = "page", name = "page",
                                     targetNamespace = "http://mpotter.noveo.com/webservice/common")
                           final Page page);

    @WebMethod
    @WebResult(name = "user", targetNamespace = "http://mpotter.noveo.com/webservice/user",
               partName = "user")
    User getUser(@WebParam(partName = "id", name = "id") Long id);

    @WebMethod
    @WebResult(name = "userId", targetNamespace = "http://mpotter.noveo.com/webservice/user",
               partName = "userId")
    long save(@WebParam(partName = "user", name = "user", targetNamespace = "http://mpotter.noveo.com/webservice/user")
              User user);

}
