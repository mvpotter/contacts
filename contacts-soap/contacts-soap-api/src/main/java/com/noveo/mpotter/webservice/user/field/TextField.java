/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 05.08.13
 * Time: 18:29
 */
package com.noveo.mpotter.webservice.user.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Text field.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "text", propOrder = { "id", "name", "value" })
public class TextField extends AbstractField {

    protected String value;

    public TextField() {
    }

    public TextField(final String name, final String value) {
        super(name);
        setValue(value);
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

}
