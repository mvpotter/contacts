/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 05.08.13
 * Time: 18:29
 */
package com.noveo.mpotter.webservice.user.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Email field.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "email")
public class EmailField extends TextField {

    public EmailField() {
    }

    public EmailField(final String name, final String value) {
        super(name, value);
    }

}
