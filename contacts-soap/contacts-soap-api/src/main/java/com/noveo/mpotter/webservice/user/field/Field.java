/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 28.10.14
 * Time: 11:02
 */
package com.noveo.mpotter.webservice.user.field;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Field object interface.
 */
@XmlTransient
@XmlSeeAlso({ TextField.class, DateField.class, EmailField.class, LinkField.class, NumberField.class })
public interface Field extends Identifiable, Nameable {
}
