/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 05.08.13
 * Time: 18:29
 */
package com.noveo.mpotter.webservice.user.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Link field.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "link")
public class LinkField extends TextField {

    public LinkField() {
    }

    public LinkField(final String name, final String value) {
        super(name, value);
    }

}
