/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 05.08.13
 * Time: 18:29
 */
package com.noveo.mpotter.webservice.user.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Number field.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "number", propOrder = { "id", "name", "number" })
public class NumberField extends AbstractField {

    private Double number;

    public NumberField() {
    }

    public NumberField(final String name, final double value) {
        super(name);
        setNumber(value);
    }

    public double getNumber() {
        return number;
    }

    public void setNumber(final Double number) {
        this.number = number;
    }

}
