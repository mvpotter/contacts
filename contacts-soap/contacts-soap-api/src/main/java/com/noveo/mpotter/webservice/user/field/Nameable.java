/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 28.10.14
 * Time: 11:01
 */
package com.noveo.mpotter.webservice.user.field;

/**
 * Nameable object interface.
 */
public interface Nameable {

    String getName();

}
