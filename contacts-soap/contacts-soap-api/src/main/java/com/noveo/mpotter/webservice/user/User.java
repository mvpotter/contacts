/*
* Created with IntelliJ IDEA.
* User: mpotter
* Date: 20.01.14
* Time: 14:58
*/
package com.noveo.mpotter.webservice.user;

import com.noveo.mpotter.webservice.address.Address;
import com.noveo.mpotter.webservice.group.Group;
import com.noveo.mpotter.webservice.user.field.Field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Contact user class.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "user", propOrder = { "id", "login", "avatar", "groups", "addresses", "fields" })
public class User {

    @XmlElement(required = true)
    private Long id;
    @XmlElement(required = true)
    private String login;
    @XmlElement(required = true)
    private byte[] avatar;
    @XmlElement(required = true)
    private Group[] groups;
    @XmlElement(required = true)
    private Address[] addresses;
    @XmlElement(required = true)
    private Field[] fields;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(final byte[] avatar) {
        this.avatar = avatar;
    }

    public Group[] getGroups() {
        return groups;
    }

    public void setGroups(final Group[] groups) {
        this.groups = groups;
    }

    public Address[] getAddresses() {
        return addresses;
    }

    public void setAddresses(final Address[] addresses) {
        this.addresses = addresses;
    }

    public Field[] getFields() {
        return fields;
    }

    public void setFields(final Field[] fields) {
        this.fields = fields;
    }

}
