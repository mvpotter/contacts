/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 05.08.13
 * Time: 18:20
 */
package com.noveo.mpotter.webservice.user.field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Base field class.
 */
@XmlTransient
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "id", "name" })
@XmlSeeAlso({ TextField.class, DateField.class, EmailField.class, LinkField.class, NumberField.class })
public abstract class AbstractField implements Field {

    protected int id;
    private String name;

    protected AbstractField() {
    }

    protected AbstractField(final String name) {
        setName(name);
    }

    public int getId() {
        return id;
    }

    public void setId(final  int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

}
