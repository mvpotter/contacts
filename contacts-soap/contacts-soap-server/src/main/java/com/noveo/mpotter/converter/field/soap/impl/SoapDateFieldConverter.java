/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.field.soap.impl;

import com.noveo.mpotter.converter.field.soap.AbstractSoapFieldConverter;
import com.noveo.mpotter.webservice.user.field.DateField;
import com.noveo.mpotter.webservice.user.field.Field;

/**
 * Class for converting model date field to SOAP API's one.
 */
public class SoapDateFieldConverter extends AbstractSoapFieldConverter {

    public SoapDateFieldConverter() {
        supportedFieldClazz = DateField.class;
    }

    @Override
    protected com.noveo.mpotter.model.field.Field convertField(final Field source) {
        final com.noveo.mpotter.model.field.impl.DateField dateField =
                new com.noveo.mpotter.model.field.impl.DateField();
        final DateField soapDateField = (DateField) source;

        dateField.setName(soapDateField.getName());
        dateField.setDate(soapDateField.getDate());

        return dateField;
    }
}
