/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 22.11.13
 * Time: 17:58
 */
package com.noveo.mpotter.webservice;

import com.noveo.mpotter.model.Group;
import com.noveo.mpotter.service.GroupService;
import com.noveo.mpotter.webservice.common.Page;
import com.noveo.mpotter.webservice.group.GroupClient;
import com.noveo.mpotter.webservice.group.GroupsList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * SOAP API service that manages groups.
 */
@Service
@WebService (name = "GroupService")
public class GroupSoapServiceImpl implements GroupClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(GroupSoapServiceImpl.class);

    private GroupService groupService;
    private ConversionService conversionService;

    public GroupSoapServiceImpl() {

    }

    @Inject
    public GroupSoapServiceImpl(final GroupService groupService, final ConversionService conversionService) {
        this.groupService = groupService;
        this.conversionService  = conversionService;
    }

    @Override
    public long save(@WebParam(partName = "group", name = "inputGroup",
                              targetNamespace = "http://mpotter.noveo.com/webservice/group")
                    final com.noveo.mpotter.webservice.group.Group group) {
        LOGGER.debug("save: id = {}, name = {}, users.size = {}",
                     new Object[]{group.getId(), group.getName(), group.getUsers().size()});
        final Group savedGroup = groupService.saveGroup(conversionService.convert(group, Group.class));
        LOGGER.debug("Saved group id = {}", savedGroup.getId());
        return savedGroup.getId();
    }

    @Override
    public GroupsList getGroupsList(@WebParam(partName = "page", name = "page",
                                              targetNamespace = "http://mpotter.noveo.com/webservice/common")
                                    final Page page) {
        LOGGER.debug("getGroupsList: page = {}, itemsOnPage = {}", page.getPage(), page.getItemsOnPage());
        final List<Group> groups = groupService.getGroupsWithUsersList(page.getPage(), page.getItemsOnPage());
        final GroupsList groupsList = conversionService.convert(groups, GroupsList.class);
        LOGGER.debug("getGroupsList: groupsList.size() = {}", groupsList.getGroups().size());
        return groupsList;
    }

}
