/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 07.07.14
 * Time: 10:04
 */
package com.noveo.mpotter.converter.field.soap;

import com.noveo.mpotter.model.field.Field;

/**
 * Abstract SOAP API  field converter to implement a chain of converters for each field type.
 */
public abstract class AbstractSoapFieldConverter implements SoapFieldConverter {

    protected Class<?> supportedFieldClazz;
    protected SoapFieldConverter nextConverter;

    public void setNext(final SoapFieldConverter converter) {
        this.nextConverter = converter;
    }

    @Override
    public Field convert(final com.noveo.mpotter.webservice.user.field.Field source) {
        if (source.getClass() == supportedFieldClazz) {
            return convertField(source);
        }
        if (nextConverter != null) {
            return nextConverter.convert(source);
        }
        return null;
    }

    protected abstract
    Field convertField(com.noveo.mpotter.webservice.user.field.Field source);
}
