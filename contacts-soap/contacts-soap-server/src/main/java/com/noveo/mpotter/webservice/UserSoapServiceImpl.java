/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 16.12.13
 * Time: 17:51
 */
package com.noveo.mpotter.webservice;

import com.noveo.mpotter.model.User;
import com.noveo.mpotter.service.UserService;
import com.noveo.mpotter.webservice.common.Page;
import com.noveo.mpotter.webservice.user.UserClient;
import com.noveo.mpotter.webservice.user.UsersList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * SOAP API service that manages users.
 */
@Service
@WebService(name = "UserService")
public class UserSoapServiceImpl implements UserClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserSoapServiceImpl.class);

    private UserService userService;
    private ConversionService conversionService;

    public UserSoapServiceImpl() {

    }

    @Inject
    public UserSoapServiceImpl(final UserService userService, final ConversionService conversionService) {
        this.userService = userService;
        this.conversionService = conversionService;
    }

    @Override
    public UsersList getUsersList(@WebParam(partName = "page", name = "page",
                                            targetNamespace = "http://mpotter.noveo.com/webservice/common")
                                  final Page page) {
        LOGGER.debug("getUsersList: page = {}, itemsOnPage = {}", page.getPage(), page.getItemsOnPage());
        final List<User> users = userService.getUsers(page.getPage(), page.getItemsOnPage());
        final UsersList usersList = conversionService.convert(users, UsersList.class);
        LOGGER.debug("getUsersList: usersList.size() = {}", usersList.getUsers().size());
        return usersList;
    }

    @Override
    public com.noveo.mpotter.webservice.user.User getUser(final Long id) {
        LOGGER.debug("getUser: user id = {}", id);
        final User user = userService.getUser(id);
        final com.noveo.mpotter.webservice.user.User soapUser =
                conversionService.convert(user, com.noveo.mpotter.webservice.user.User.class);
        LOGGER.debug("getUser: user login = {}", soapUser != null ? soapUser.getLogin() : null);
        return soapUser;
    }

    @Override
    public long save(@WebParam(partName = "parameters", name = "user",
                              targetNamespace = "http://mpotter.noveo.com/webservice/user")
                    final com.noveo.mpotter.webservice.user.User user) {
        LOGGER.debug("save: user login = {}, avatar not null = {}, ",
                     new Object[]{user.getLogin(), user.getAvatar() != null});
        final User newUser = userService.saveUser(conversionService.convert(user, User.class));
        LOGGER.debug("save: result code = {}", 0);
        return newUser.getId();
    }

}
