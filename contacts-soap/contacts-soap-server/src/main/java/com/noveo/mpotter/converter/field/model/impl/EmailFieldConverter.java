/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.field.model.impl;

import com.noveo.mpotter.converter.field.model.AbstractFieldConverter;
import com.noveo.mpotter.model.field.impl.EmailField;
import com.noveo.mpotter.webservice.user.field.Field;

/**
 * Class for converting model email field to SOAP API's one.
 */
public class EmailFieldConverter extends AbstractFieldConverter {

    public EmailFieldConverter() {
        supportedFieldClazz = EmailField.class;
    }

    @Override
    protected Field convertField(final com.noveo.mpotter.model.field.Field source) {
        final com.noveo.mpotter.webservice.user.field.EmailField soapEmailField =
                new com.noveo.mpotter.webservice.user.field.EmailField();
        final EmailField emailField = (EmailField) source;

        soapEmailField.setName(emailField.getName());
        soapEmailField.setValue(emailField.getValue());

        return soapEmailField;
    }
}
