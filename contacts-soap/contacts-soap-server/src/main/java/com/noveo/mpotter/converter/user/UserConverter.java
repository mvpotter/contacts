/**
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.user;

import com.noveo.mpotter.converter.address.AddressListConverter;
import com.noveo.mpotter.converter.field.model.FieldConverter;
import com.noveo.mpotter.converter.field.model.FieldConverterChainFactory;
import com.noveo.mpotter.model.Address;
import com.noveo.mpotter.model.Avatar;
import com.noveo.mpotter.model.Group;
import com.noveo.mpotter.model.User;
import com.noveo.mpotter.model.field.Field;
import org.springframework.core.convert.converter.Converter;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static com.noveo.mpotter.converter.ConverterUtils.toSoapId;

/**
 * Class for converting User model object to SOAP API one.
 */
public class UserConverter implements Converter<User, com.noveo.mpotter.webservice.user.User> {

    private static final AddressListConverter ADDRESS_LIST_CONVERTER = new AddressListConverter();
    private static final FieldConverter FIELD_CONVERTER = FieldConverterChainFactory.createChain();

    @Override
    public com.noveo.mpotter.webservice.user.User convert(final User user) {
        final com.noveo.mpotter.webservice.user.User soapUser = new com.noveo.mpotter.webservice.user.User();

        soapUser.setId(toSoapId(user.getId()));
        soapUser.setLogin(user.getLogin());
        final Avatar avatar = user.getAvatar();
        if (avatar != null) {
            soapUser.setAvatar(avatar.getImage());
        }
        setGroups(soapUser, user.getGroups());
        setAddresses(soapUser, user.getAddresses());
        setFields(soapUser, user.getAdditionalFields());

        return soapUser;
    }

    private void setGroups(final com.noveo.mpotter.webservice.user.User soapUser, final Set<Group> groups) {
        if (groups != null) {
            final List<com.noveo.mpotter.webservice.group.Group> soapGroups =
                    new LinkedList<com.noveo.mpotter.webservice.group.Group>();
            for (Group group: groups) {
                final com.noveo.mpotter.webservice.group.Group soapGroup =
                        new com.noveo.mpotter.webservice.group.Group();
                soapGroup.setId(group.getId());
                soapGroup.setName(group.getName());
                soapGroups.add(soapGroup);
            }
            soapUser.setGroups(soapGroups.toArray(new com.noveo.mpotter.webservice.group.Group[0]));
        }
    }

    private void setAddresses(final com.noveo.mpotter.webservice.user.User soapUser, final Set<Address> addresses) {
        if (addresses != null) {
            final List<com.noveo.mpotter.webservice.address.Address> soapAddresses =
                    ADDRESS_LIST_CONVERTER.convert(addresses).getAddresses();
            soapUser.setAddresses(soapAddresses.toArray(new com.noveo.mpotter.webservice.address.Address[0]));
        }
    }

    private void setFields(final com.noveo.mpotter.webservice.user.User soapUser,
                           final Set<Field> fields) {
        if (fields != null) {
            final List<com.noveo.mpotter.webservice.user.field.Field> soapFields =
                    new LinkedList<com.noveo.mpotter.webservice.user.field.Field>();
            for (Field field: fields) {
                final com.noveo.mpotter.webservice.user.field.Field soapField =
                        FIELD_CONVERTER.convert(field);
                if (soapField != null) {
                    soapFields.add(soapField);
                }
            }
            final com.noveo.mpotter.webservice.user.field.Field[] soapFieldsArray =
                    soapFields.toArray(new com.noveo.mpotter.webservice.user.field.Field[0]);
            soapUser.setFields(soapFieldsArray);
        }
    }

}
