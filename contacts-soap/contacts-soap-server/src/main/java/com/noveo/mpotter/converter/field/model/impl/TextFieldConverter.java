/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.field.model.impl;

import com.noveo.mpotter.converter.field.model.AbstractFieldConverter;
import com.noveo.mpotter.model.field.impl.TextField;
import com.noveo.mpotter.webservice.user.field.Field;

/**
 * Class for converting model text field to SOAP API's one.
 */
public class TextFieldConverter extends AbstractFieldConverter {

    public TextFieldConverter() {
        supportedFieldClazz = TextField.class;
    }

    @Override
    protected Field convertField(final com.noveo.mpotter.model.field.Field source) {
        final com.noveo.mpotter.webservice.user.field.TextField soapTextField =
                new com.noveo.mpotter.webservice.user.field.TextField();
        final TextField textField = (TextField) source;

        soapTextField.setName(textField.getName());
        soapTextField.setValue(textField.getValue());

        return soapTextField;
    }
}
