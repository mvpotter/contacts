/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.address;

import com.noveo.mpotter.model.Address;
import com.noveo.mpotter.webservice.address.AddressesList;
import org.springframework.core.convert.converter.Converter;

import java.util.Collection;

/**
 * Class for converting model address list to a list of SOAP API's address objects.
 */
public class AddressListConverter implements Converter<Collection<Address>, AddressesList> {

    @Override
    public AddressesList convert(final Collection<Address> addresses) {
        final AddressConverter addressConverter = new AddressConverter();
        final AddressesList addressesList = new AddressesList();

        for (Address address: addresses) {
            addressesList.getAddresses().add(addressConverter.convert(address));
        }

        return addressesList;
    }

}
