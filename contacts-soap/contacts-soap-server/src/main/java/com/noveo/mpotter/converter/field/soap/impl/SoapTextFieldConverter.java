/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.field.soap.impl;

import com.noveo.mpotter.converter.field.soap.AbstractSoapFieldConverter;
import com.noveo.mpotter.webservice.user.field.Field;
import com.noveo.mpotter.webservice.user.field.TextField;

/**
 * Class for converting model text field to SOAP API's one.
 */
public class SoapTextFieldConverter extends AbstractSoapFieldConverter {

    public SoapTextFieldConverter() {
        supportedFieldClazz = TextField.class;
    }

    @Override
    protected com.noveo.mpotter.model.field.Field convertField(final Field source) {
        final com.noveo.mpotter.model.field.impl.TextField textField =
                new com.noveo.mpotter.model.field.impl.TextField();
        final TextField soapTextField = (TextField) source;

        textField.setName(soapTextField.getName());
        textField.setValue(soapTextField.getValue());

        return textField;
    }
}
