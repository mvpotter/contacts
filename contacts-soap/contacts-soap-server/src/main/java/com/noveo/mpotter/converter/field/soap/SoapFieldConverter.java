/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 07.07.14
 * Time: 10:17
 */
package com.noveo.mpotter.converter.field.soap;

import com.noveo.mpotter.model.field.Field;
import org.springframework.core.convert.converter.Converter;

/**
 * Inteface for SOAP API field converters.
 */
public interface SoapFieldConverter extends
        Converter<com.noveo.mpotter.webservice.user.field.Field, Field> {

    void setNext(SoapFieldConverter converter);

}
