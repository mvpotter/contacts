/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.field.model.impl;

import com.noveo.mpotter.converter.field.model.AbstractFieldConverter;
import com.noveo.mpotter.model.field.impl.NumberField;
import com.noveo.mpotter.webservice.user.field.Field;

/**
 * Class for converting model number field to SOAP API's one.
 */
public class NumberFieldConverter extends AbstractFieldConverter {

    public NumberFieldConverter() {
        supportedFieldClazz = NumberField.class;
    }

    @Override
    protected Field convertField(final com.noveo.mpotter.model.field.Field source) {
        final com.noveo.mpotter.webservice.user.field.NumberField soapNumberField =
                new com.noveo.mpotter.webservice.user.field.NumberField();
        final NumberField numberField = (NumberField) source;

        soapNumberField.setName(numberField.getName());
        soapNumberField.setNumber(numberField.getNumber());

        return soapNumberField;
    }
}
