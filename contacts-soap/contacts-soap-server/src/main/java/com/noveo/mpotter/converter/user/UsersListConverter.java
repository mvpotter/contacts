/**
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.user;

import com.noveo.mpotter.model.User;
import com.noveo.mpotter.webservice.user.UsersList;
import org.springframework.core.convert.converter.Converter;

import java.util.List;

/**
 * Class for converting User model objects list to SOAP API one.
 */
public class UsersListConverter implements Converter<List<User>, UsersList> {

    @Override
    public UsersList convert(final List<User> users) {
        final UserConverter userConverter = new UserConverter();
        final UsersList usersList = new UsersList();

        for (User user: users) {
            usersList.getUsers().add(userConverter.convert(user));
        }

        return usersList;
    }

}
