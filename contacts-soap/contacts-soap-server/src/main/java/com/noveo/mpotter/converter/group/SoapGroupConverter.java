/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.group;

import com.noveo.mpotter.model.Group;
import org.springframework.core.convert.converter.Converter;

/**
 * Class for converting SOAP group to model group object.
 */
public class SoapGroupConverter implements Converter<com.noveo.mpotter.webservice.group.Group, Group> {

    @Override
    public Group convert(final com.noveo.mpotter.webservice.group.Group soapGroup) {
        final Group group = new Group();

        group.setId(soapGroup.getId());
        group.setName(soapGroup.getName());

        return group;
    }

}
