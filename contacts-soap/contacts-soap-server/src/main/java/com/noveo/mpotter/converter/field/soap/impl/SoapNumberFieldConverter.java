/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.field.soap.impl;

import com.noveo.mpotter.converter.field.soap.AbstractSoapFieldConverter;
import com.noveo.mpotter.webservice.user.field.Field;
import com.noveo.mpotter.webservice.user.field.NumberField;

/**
 * Class for converting model number field to SOAP API's one.
 */
public class SoapNumberFieldConverter extends AbstractSoapFieldConverter {

    public SoapNumberFieldConverter() {
        supportedFieldClazz = NumberField.class;
    }

    @Override
    protected com.noveo.mpotter.model.field.Field convertField(final Field source) {
        final com.noveo.mpotter.model.field.impl.NumberField numberField =
                new com.noveo.mpotter.model.field.impl.NumberField();
        final NumberField soapNumberField = (NumberField) source;

        numberField.setName(soapNumberField.getName());
        numberField.setNumber(soapNumberField.getNumber());

        return numberField;
    }
}
