/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 07.07.14
 * Time: 11:29
 */
package com.noveo.mpotter.converter.field.soap;

import com.noveo.mpotter.converter.field.soap.impl.SoapDateFieldConverter;
import com.noveo.mpotter.converter.field.soap.impl.SoapEmailFieldConverter;
import com.noveo.mpotter.converter.field.soap.impl.SoapLinkFieldConverter;
import com.noveo.mpotter.converter.field.soap.impl.SoapNumberFieldConverter;
import com.noveo.mpotter.converter.field.soap.impl.SoapTextFieldConverter;

/**
 * Factory for creating SOAP API field converter chain.
 */
public abstract class SoapFieldConverterChainFactory {

    private SoapFieldConverterChainFactory() {

    }

    public static SoapFieldConverter createChain() {

        final SoapFieldConverter textSoapFieldConverter = new SoapTextFieldConverter();

        final SoapFieldConverter emailSoapFieldConverter = new SoapEmailFieldConverter();
        emailSoapFieldConverter.setNext(textSoapFieldConverter);

        final SoapFieldConverter linkSoapFieldConverter = new SoapLinkFieldConverter();
        linkSoapFieldConverter.setNext(emailSoapFieldConverter);

        final SoapFieldConverter numberSoapFieldConverter = new SoapNumberFieldConverter();
        numberSoapFieldConverter.setNext(linkSoapFieldConverter);

        final SoapFieldConverter converter = new SoapDateFieldConverter();
        converter.setNext(numberSoapFieldConverter);

        return converter;
    }

}
