/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 20.01.14
 * Time: 15:55
 */
package com.noveo.mpotter.converter;

/**
 * Utils class with common converting operations.
 */
public final class ConverterUtils {

    private ConverterUtils() {

    }

    public static Long toEntityId(final long id) {
        if (id <= 0) {
            return null;
        }

        return id;
    }

    public static long toSoapId(final Long id) {
        if (id == null) {
            return 0;
        }

        return id;
    }

}
