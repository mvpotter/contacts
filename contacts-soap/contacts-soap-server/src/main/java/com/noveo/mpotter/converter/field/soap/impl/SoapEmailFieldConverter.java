/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.field.soap.impl;

import com.noveo.mpotter.converter.field.soap.AbstractSoapFieldConverter;
import com.noveo.mpotter.webservice.user.field.EmailField;
import com.noveo.mpotter.webservice.user.field.Field;

/**
 * Class for converting model email field to SOAP API's one.
 */
public class SoapEmailFieldConverter extends AbstractSoapFieldConverter {

    public SoapEmailFieldConverter() {
        supportedFieldClazz = EmailField.class;
    }

    @Override
    protected com.noveo.mpotter.model.field.Field convertField(final Field source) {
        final com.noveo.mpotter.model.field.impl.EmailField emailField =
                new com.noveo.mpotter.model.field.impl.EmailField();
        final EmailField soapEmailField = (EmailField) source;

        emailField.setName(soapEmailField.getName());
        emailField.setValue(soapEmailField.getValue());

        return emailField;
    }
}
