/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.group;

import com.noveo.mpotter.model.Group;
import com.noveo.mpotter.webservice.group.GroupsList;
import org.springframework.core.convert.converter.Converter;

import java.util.List;

/**
 * Class for converting SOAP group list to a list of model group objects.
 */
public class GroupListConverter implements Converter<List<Group>, GroupsList> {

    @Override
    public GroupsList convert(final List<Group> groups) {
        final GroupConverter groupConverter = new GroupConverter();
        final GroupsList groupsList = new GroupsList();

        for (Group group: groups) {
            groupsList.getGroups().add(groupConverter.convert(group));
        }

        return groupsList;
    }

}
