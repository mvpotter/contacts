/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.field.model.impl;

import com.noveo.mpotter.converter.field.model.AbstractFieldConverter;
import com.noveo.mpotter.model.field.impl.DateField;
import com.noveo.mpotter.webservice.user.field.Field;

/**
 * Class for converting model date field to SOAP API's one.
 */
public class DateFieldConverter extends AbstractFieldConverter {

    public DateFieldConverter() {
        supportedFieldClazz = DateField.class;
    }

    @Override
    protected Field convertField(final com.noveo.mpotter.model.field.Field source) {
        final com.noveo.mpotter.webservice.user.field.DateField soapDateField =
                new com.noveo.mpotter.webservice.user.field.DateField();
        final DateField dateField = (DateField) source;

        soapDateField.setName(dateField.getName());
        soapDateField.setDate(dateField.getDate());

        return soapDateField;
    }
}
