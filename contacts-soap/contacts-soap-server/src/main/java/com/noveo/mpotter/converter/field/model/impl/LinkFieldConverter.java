/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.field.model.impl;

import com.noveo.mpotter.converter.field.model.AbstractFieldConverter;
import com.noveo.mpotter.model.field.impl.LinkField;
import com.noveo.mpotter.webservice.user.field.Field;

/**
 * Class for converting model link field to SOAP API's one.
 */
public class LinkFieldConverter extends AbstractFieldConverter {

    public LinkFieldConverter() {
        supportedFieldClazz = LinkField.class;
    }

    @Override
    protected Field convertField(final com.noveo.mpotter.model.field.Field source) {
        final com.noveo.mpotter.webservice.user.field.LinkField soapLinkField =
                new com.noveo.mpotter.webservice.user.field.LinkField();
        final LinkField linkField = (LinkField) source;

        soapLinkField.setName(linkField.getName());
        soapLinkField.setValue(linkField.getValue());

        return soapLinkField;
    }
}
