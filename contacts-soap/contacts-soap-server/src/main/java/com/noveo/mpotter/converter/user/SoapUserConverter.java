/**
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.user;

import com.noveo.mpotter.converter.address.SoapAddressListConverter;
import com.noveo.mpotter.converter.field.soap.SoapFieldConverter;
import com.noveo.mpotter.converter.field.soap.SoapFieldConverterChainFactory;
import com.noveo.mpotter.model.Address;
import com.noveo.mpotter.model.Avatar;
import com.noveo.mpotter.model.Group;
import com.noveo.mpotter.model.User;
import com.noveo.mpotter.model.field.Field;
import org.springframework.core.convert.converter.Converter;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static com.noveo.mpotter.converter.ConverterUtils.toEntityId;

/**
 * Class for converting User SOAP API object to model one.
 */
public class SoapUserConverter implements Converter<com.noveo.mpotter.webservice.user.User, User> {

    private static final SoapAddressListConverter ADDRESS_LIST_CONVERTER = new SoapAddressListConverter();
    private static final SoapFieldConverter FIELD_CONVERTER = SoapFieldConverterChainFactory.createChain();

    @Override
    public User convert(final com.noveo.mpotter.webservice.user.User soapUser) {
        final User user = new User();

        user.setId(toEntityId(soapUser.getId()));
        user.setLogin(soapUser.getLogin());
        user.setAvatar(new Avatar(soapUser.getAvatar()));
        setGroups(user, soapUser.getGroups());
        setAddresses(user, soapUser.getAddresses());
        setFields(user, soapUser.getFields());

        return user;
    }

    private void setGroups(final User user, final com.noveo.mpotter.webservice.group.Group[] soapGroups) {
        if (soapGroups != null) {
            final List<Group> groups = new LinkedList<Group>();
            for (com.noveo.mpotter.webservice.group.Group soapGroup: soapGroups) {
                final Group group = new Group();
                group.setId(soapGroup.getId());
                group.setName(soapGroup.getName());
                groups.add(group);
            }
            user.setGroups(new HashSet<Group>(groups));
        }
    }

    private void setAddresses(final User user, final com.noveo.mpotter.webservice.address.Address[] soapAddresses) {
        if (soapAddresses != null) {
            final Collection<Address> addresses = ADDRESS_LIST_CONVERTER.convert(soapAddresses);
            user.setAddresses(new LinkedHashSet<Address>(addresses));
        }
    }

    private void setFields(final User user,
                           final com.noveo.mpotter.webservice.user.field.Field[] soapFields) {
        if (soapFields != null) {
            final Set<Field> fields = new LinkedHashSet<Field>();
            for (com.noveo.mpotter.webservice.user.field.Field soapField: soapFields) {
                // FIXME: casting to Field is a bit dangerous here. Find better solution.
                final Field field = (Field) FIELD_CONVERTER.convert(soapField);
                if (field != null) {
                    fields.add(field);
                }
            }
            user.setAdditionalFields(fields);
        }
    }

}
