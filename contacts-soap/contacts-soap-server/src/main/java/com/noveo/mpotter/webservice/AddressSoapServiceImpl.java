/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 22.11.13
 * Time: 17:58
 */
package com.noveo.mpotter.webservice;

import com.noveo.mpotter.model.Address;
import com.noveo.mpotter.service.AddressService;
import com.noveo.mpotter.webservice.address.AddressClient;
import com.noveo.mpotter.webservice.address.AddressesList;
import com.noveo.mpotter.webservice.common.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * SOAP API service that manages addresses.
 */
@Service
@WebService (name = "AddressService")
public class AddressSoapServiceImpl implements AddressClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddressSoapServiceImpl.class);

    private AddressService addressService;
    private ConversionService conversionService;

    public AddressSoapServiceImpl() {

    }

    @Inject
    public AddressSoapServiceImpl(final AddressService addressService, final ConversionService conversionService) {
        this.addressService = addressService;
        this.conversionService = conversionService;
    }

    @Override
    public AddressesList getAddressesList(@WebParam(partName = "page", name = "page",
                                                    targetNamespace = "http://mpotter.noveo.com/webservice/common")
                                          final Page page) {
        LOGGER.debug("getAddressesList: page = {}, itemsOnPage = {}", page.getPage(), page.getItemsOnPage());
        final List<Address> addresses = addressService.getAddresses(page.getPage(), page.getItemsOnPage());
        final AddressesList addressesList = conversionService.convert(addresses, AddressesList.class);
        LOGGER.debug("getAddressesList: addressesList.size() = {}", addressesList.getAddresses().size());
        return addressesList;
    }

    @Override
    public int add(@WebParam(partName = "address", name = "inputAddress",
                             targetNamespace = "http://mpotter.noveo.com/webservice/address")
                   final com.noveo.mpotter.webservice.address.Address address) {
        LOGGER.debug("add: type = {}, city = {}, street = {}",
                     new Object[]{address.getType(), address.getCity(), address.getStreet()});
        addressService.addAddress(conversionService.convert(address, Address.class));
        LOGGER.debug("add: result code = {}", 0);
        return 0;
    }

}
