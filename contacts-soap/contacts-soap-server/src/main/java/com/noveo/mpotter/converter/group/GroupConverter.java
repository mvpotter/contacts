/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.group;

import com.noveo.mpotter.model.Group;
import com.noveo.mpotter.model.User;
import com.noveo.mpotter.webservice.group.GroupUser;
import org.springframework.core.convert.converter.Converter;

import java.util.Set;

/**
 * Class for converting model group to SOAP group object.
 */
public class GroupConverter implements Converter<Group, com.noveo.mpotter.webservice.group.Group> {

    @Override
    public com.noveo.mpotter.webservice.group.Group convert(final Group group) {
        final com.noveo.mpotter.webservice.group.Group soapGroup = new com.noveo.mpotter.webservice.group.Group();

        soapGroup.setId(group.getId());
        soapGroup.setName(group.getName());
        final Set<User> users = group.getUsers();
        if (users != null) {
            for (User user: users) {
                final GroupUser soapUser = new com.noveo.mpotter.webservice.group.GroupUser();
                soapUser.setId(user.getId());
                soapUser.setName(user.getLogin());
                soapGroup.getUsers().add(soapUser);
            }
        }

        return soapGroup;
    }

}
