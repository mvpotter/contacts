/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 07.07.14
 * Time: 11:29
 */
package com.noveo.mpotter.converter.field.model;

import com.noveo.mpotter.converter.field.model.impl.DateFieldConverter;
import com.noveo.mpotter.converter.field.model.impl.EmailFieldConverter;
import com.noveo.mpotter.converter.field.model.impl.LinkFieldConverter;
import com.noveo.mpotter.converter.field.model.impl.NumberFieldConverter;
import com.noveo.mpotter.converter.field.model.impl.TextFieldConverter;

/**
 * Factory for creating field converter chain.
 */
public abstract class FieldConverterChainFactory {

    private FieldConverterChainFactory() {

    }

    public static FieldConverter createChain() {

        final FieldConverter textFieldConverter = new TextFieldConverter();

        final FieldConverter emailFieldConverter = new EmailFieldConverter();
        emailFieldConverter.setNext(textFieldConverter);

        final FieldConverter linkFieldConverter = new LinkFieldConverter();
        linkFieldConverter.setNext(emailFieldConverter);

        final FieldConverter numberFieldConverter = new NumberFieldConverter();
        numberFieldConverter.setNext(linkFieldConverter);

        final FieldConverter converter = new DateFieldConverter();
        converter.setNext(numberFieldConverter);

        return converter;
    }

}
