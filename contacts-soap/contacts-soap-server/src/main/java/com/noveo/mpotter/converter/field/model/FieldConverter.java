/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 07.07.14
 * Time: 10:17
 */
package com.noveo.mpotter.converter.field.model;

import com.noveo.mpotter.model.field.Field;
import org.springframework.core.convert.converter.Converter;

/**
 * Inteface for field converters.
 */
public interface FieldConverter extends
        Converter<Field, com.noveo.mpotter.webservice.user.field.Field> {

    void setNext(FieldConverter converter);

}
