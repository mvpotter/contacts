/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 07.07.14
 * Time: 10:04
 */
package com.noveo.mpotter.converter.field.model;

import com.noveo.mpotter.model.field.Field;

/**
 * Abstract field converter to implement a chain of converters for each field type.
 */
public abstract class AbstractFieldConverter implements FieldConverter {

    protected Class<?> supportedFieldClazz;
    protected FieldConverter nextConverter;

    public void setNext(final FieldConverter converter) {
        this.nextConverter = converter;
    }

    @Override
    public com.noveo.mpotter.webservice.user.field.Field convert(final Field source) {
        if (source.getClass() == supportedFieldClazz) {
            return convertField(source);
        }
        if (nextConverter != null) {
            return nextConverter.convert(source);
        }
        return null;
    }

    protected abstract com.noveo.mpotter.webservice.user.field.Field convertField(Field source);
}
