/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.address;

import com.noveo.mpotter.model.Address;
import org.springframework.core.convert.converter.Converter;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Class for converting SOAP address list to a list of model address objects.
 */
public class SoapAddressListConverter
        implements Converter<com.noveo.mpotter.webservice.address.Address[], Collection<Address>> {

    public static final SoapAddressConverter ADDRESS_CONVERTER = new SoapAddressConverter();

    @Override
    public Collection<Address> convert(final com.noveo.mpotter.webservice.address.Address[] soapAddresses) {
        final List<Address> addresses = new LinkedList<Address>();

        for (com.noveo.mpotter.webservice.address.Address soapAddress: soapAddresses) {
            addresses.add(ADDRESS_CONVERTER.convert(soapAddress));
        }

        return addresses;
    }

}
