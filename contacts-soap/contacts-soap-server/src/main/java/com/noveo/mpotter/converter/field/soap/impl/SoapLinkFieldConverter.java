/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.field.soap.impl;

import com.noveo.mpotter.converter.field.soap.AbstractSoapFieldConverter;
import com.noveo.mpotter.webservice.user.field.Field;
import com.noveo.mpotter.webservice.user.field.LinkField;

/**
 * Class for converting model link field to SOAP API's one.
 */
public class SoapLinkFieldConverter extends AbstractSoapFieldConverter {

    public SoapLinkFieldConverter() {
        supportedFieldClazz = LinkField.class;
    }

    @Override
    protected com.noveo.mpotter.model.field.Field convertField(final Field source) {
        final com.noveo.mpotter.model.field.impl.LinkField linkField =
                new com.noveo.mpotter.model.field.impl.LinkField();
        final LinkField soapLinkField = (LinkField) source;

        linkField.setName(soapLinkField.getName());
        linkField.setValue(soapLinkField.getValue());

        return linkField;
    }
}
