/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.address;

import com.noveo.mpotter.model.Address;
import com.noveo.mpotter.model.field.impl.AddressType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

/**
 * Class for converting SOAP address to model address object.
 */
public class SoapAddressConverter implements Converter<com.noveo.mpotter.webservice.address.Address, Address> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SoapAddressConverter.class);

    @Override
    public Address convert(final com.noveo.mpotter.webservice.address.Address soapAddress) {
        final Address address = new Address();

        AddressType type = AddressType.OTHER;
        try {
            type = AddressType.valueOf(soapAddress.getType());
        } catch (IllegalArgumentException e) {
            LOGGER.debug("Unknown AddressType: \"{}\"", soapAddress.getType());
        }

        address.setType(type);
        address.setCity(soapAddress.getCity());
        address.setStreet(soapAddress.getStreet());

        return address;
    }

}
