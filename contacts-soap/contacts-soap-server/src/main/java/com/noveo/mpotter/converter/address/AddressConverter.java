/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.converter.address;

import com.noveo.mpotter.model.Address;
import org.springframework.core.convert.converter.Converter;

/**
 * Class for converting model address to SOAP address object.
 */
public class AddressConverter implements Converter<Address, com.noveo.mpotter.webservice.address.Address> {

    @Override
    public com.noveo.mpotter.webservice.address.Address convert(final Address address) {
        final com.noveo.mpotter.webservice.address.Address soapAddress =
                new com.noveo.mpotter.webservice.address.Address();

        soapAddress.setType(address.getType().name());
        soapAddress.setCity(address.getCity());
        soapAddress.setStreet(address.getStreet());

        return soapAddress;
    }

}
