<%@ page import="com.noveo.mpotter.model.User" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hello</title>
</head>
<body>
<% List<User> users = (List<User>) request.getAttribute("users");
    for (User user : users) {
        response.getWriter().write("<p>" + user.toString());
    }

%>
</body>
</html>
