package com.noveo.mpotter.controller;

import com.google.common.io.ByteStreams;
import com.noveo.mpotter.model.Address;
import com.noveo.mpotter.model.Avatar;
import com.noveo.mpotter.model.Group;
import com.noveo.mpotter.model.User;
import com.noveo.mpotter.model.field.impl.AddressType;
import com.noveo.mpotter.model.field.impl.DateField;
import com.noveo.mpotter.model.field.impl.EmailField;
import com.noveo.mpotter.model.field.impl.LinkField;
import com.noveo.mpotter.model.field.impl.NumberField;
import com.noveo.mpotter.model.field.impl.TextField;
import com.noveo.mpotter.service.GroupService;
import com.noveo.mpotter.service.UserService;
import org.slf4j.Logger;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

/**
 * Initializes database.
 */
@Controller
public class JPAInitializator  {

    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(JPAInitializator.class);

    private static final String NOVOSIBIRSK = "Novosibirsk";
    private static final int AGE = 25;
    private static final String POTTER_AVATAR = "/com/noveo/mpotter/images/potter.jpg";

    private UserService userService;
    private GroupService groupService;

    @Inject
    public JPAInitializator(final UserService userService, final GroupService groupService) {
        this.userService = userService;
        this.groupService = groupService;
    }

    @PostConstruct
    public void initDatabase() {
        final Group noveoGroup = groupService.saveGroup(new Group("Noveo"));
        final Group javaGroup = groupService.saveGroup(new Group("Java"));
        groupService.saveGroup(new Group("PHP"));

        final Address homeAddress = new Address();
        homeAddress.setCity(NOVOSIBIRSK);
        homeAddress.setStreet("Pirogova 34");
        homeAddress.setType(AddressType.HOME);

        final Address workAddress = new Address();
        workAddress.setCity(NOVOSIBIRSK);
        workAddress.setStreet("Razyezdnaya 12");
        workAddress.setType(AddressType.WORK);

        final User user = new User();
        user.setLogin("mpotter");
        final InputStream inputStream = JPAInitializator.class.getResourceAsStream(POTTER_AVATAR);
        try {
            user.setAvatar(new Avatar(ByteStreams.toByteArray(inputStream)));
        } catch (IOException e) {
            LOGGER.warn(e.getMessage());
            LOGGER.debug(e.getMessage(), e);
        }

        user.addGroup(noveoGroup);
        user.addGroup(javaGroup);

        user.addAddress(homeAddress);
        user.addAddress(workAddress);

        user.addAdditionalField(new DateField("Birthday", new Date()));
        user.addAdditionalField(new EmailField("work", "mpotter@noveogroup.com"));
        user.addAdditionalField(new LinkField("wiki profile",
                "https://companystaff.noveogroup.com/employee/view/mpotter"));
        user.addAdditionalField(new NumberField("age", AGE));
        user.addAdditionalField(new TextField("Position", "Java Developer"));

        userService.saveUser(user);
    }

}
