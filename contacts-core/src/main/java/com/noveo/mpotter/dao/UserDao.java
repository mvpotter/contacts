/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 30.09.13
 * Time: 16:44
 */
package com.noveo.mpotter.dao;

import com.noveo.mpotter.model.User;

/**
 * User dao interface.
 */
public interface UserDao extends BaseDao<User> {

    User findOneEager(Long id);

}
