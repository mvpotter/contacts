/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 01.10.13
 * Time: 16:58
 * @param <T> entity type
 */
package com.noveo.mpotter.dao.impl;

import com.noveo.mpotter.dao.BaseDao;
import org.springframework.util.StringUtils;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Base dao class.
 *
 * @param <T> entity type
 */
public abstract class AbstractBaseDaoImpl<T> implements BaseDao<T> {

    public static final String DELETE_ALL_QUERY = "delete from %s";

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public List<T> findAll(final int page, final int itemsOnPage) {
        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        final Root<T> root = criteriaQuery.from(getEntityClass());
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).
                             setFirstResult(page * itemsOnPage).setMaxResults(itemsOnPage).
                             getResultList();
    }

    @Override
    public T findOne(final Object id) {
        return entityManager.find(getEntityClass(), id);
    }

    @Override
    public T save(final T entity) {
        return entityManager.merge(entity);
    }

    @Override
    public Collection<T> save(final Collection<T> entities) {
        final List<T> result = new ArrayList<T>();

        if (entities == null) {
            return result;
        }

        for (T entity : entities) {
            result.add(save(entity));
        }

        return result;
    }

    @Override
    public void delete(final T entity) {
        final T managedEntity = entityManager.merge(entity);
        entityManager.remove(managedEntity);
    }

    @Override
    public void deleteAll() {
        final Entity entity = getEntityClass().getAnnotation(Entity.class);
        final boolean hasName = (entity != null) && StringUtils.hasText(entity.name());
        final String entityName = hasName ? entity.name() : getEntityClass().getSimpleName();
        entityManager.createQuery(String.format(DELETE_ALL_QUERY, entityName)).executeUpdate();
        entityManager.clear();
    }

    protected abstract Class<T> getEntityClass();

}
