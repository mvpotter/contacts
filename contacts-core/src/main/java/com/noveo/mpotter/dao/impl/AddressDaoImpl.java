/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 02.10.13
 * Time: 16:34
 */
package com.noveo.mpotter.dao.impl;

import com.noveo.mpotter.dao.AddressDao;
import com.noveo.mpotter.model.Address;
import org.springframework.stereotype.Repository;

/**
 * Address dao.
 */
@Repository
public class AddressDaoImpl extends AbstractBaseDaoImpl<Address> implements AddressDao {

    @Override
    protected Class<Address> getEntityClass() {
        return Address.class;
    }

}
