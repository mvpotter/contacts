/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 18.10.13
 * Time: 17:33
 */
package com.noveo.mpotter.dao.impl;

import com.noveo.mpotter.dao.FieldDao;
import com.noveo.mpotter.model.field.impl.AbstractField;
import org.springframework.stereotype.Repository;

/**
 * Field dao.
 */
@Repository
public class FieldDaoImpl extends AbstractBaseDaoImpl<AbstractField> implements FieldDao {

    @Override
    protected Class<AbstractField> getEntityClass() {
        return AbstractField.class;
    }

}
