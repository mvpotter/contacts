/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 18.10.13
 * Time: 17:33
 */
package com.noveo.mpotter.dao.impl;

import com.noveo.mpotter.dao.UserDao;
import com.noveo.mpotter.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

/**
 * User dao.
 */
@Repository
public class UserDaoImpl extends AbstractBaseDaoImpl<User> implements UserDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

    private static final String LOGIN = "login";
    private static final String AVATAR = "avatar";
    private static final String GROUPS = "groups";
    private static final String ADDRESSES = "addresses";
    private static final String ADDITIONAL_FIELDS = "additionalFields";

    private static final String ID = "id";

    @Override
    public User findOneEager(final Long id) {
        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        final Root<User> root = criteriaQuery.from(getEntityClass());
        root.fetch(AVATAR, JoinType.LEFT);
        root.fetch(GROUPS, JoinType.LEFT);
        root.fetch(ADDRESSES, JoinType.LEFT);
        root.fetch(ADDITIONAL_FIELDS, JoinType.LEFT);
        criteriaQuery.where(criteriaBuilder.equal(root.get(ID), id));
        criteriaQuery.select(root);
        criteriaQuery.orderBy(criteriaBuilder.asc(root.get(LOGIN)),
                              criteriaBuilder.asc(root.get(LOGIN)),
                              criteriaBuilder.asc(root.get(LOGIN)));
        User user = null;
        try {
            user = entityManager.createQuery(criteriaQuery).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.debug(e.getMessage());
        }
        return user;
    }

    @Override
    protected Class<User> getEntityClass() {
        return User.class;
    }

}
