/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 30.09.13
 * Time: 16:42
 * @param <T> entity type
 */
package com.noveo.mpotter.dao;

import java.util.Collection;
import java.util.List;

/**
 * Base dao interface.
 *
 * @param <T> entity type
 */
public interface BaseDao<T> {

    List<T> findAll(int page, int itemsOnPage);
    T findOne(Object id);
    T save(T entity);
    Collection<T> save(Collection<T> entities);
    void delete(T entity);
    void deleteAll();

}
