/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 02.10.13
 * Time: 16:34
 */
package com.noveo.mpotter.dao.impl;

import com.noveo.mpotter.dao.AddressDao;
import com.noveo.mpotter.model.Address;
import org.springframework.util.StringUtils;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Address dao. Used as an example of application transaction management.
 */
public class AddressDaoImplWithManualTransactionManagement implements AddressDao {

    public static final String DELETE_ALL_QUERY = "delete from %s";

    private final EntityManager entityManager;

    public AddressDaoImplWithManualTransactionManagement(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Address> findAll(final int page, final int itemsOnPage) {
        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Address> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        final Root<Address> root = criteriaQuery.from(getEntityClass());
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).
                setFirstResult(page * itemsOnPage).setMaxResults(itemsOnPage).
                getResultList();
    }

    /**
     * An example of native SQL query execution.
     *
     * @param id entity id
     * @return address or null, if not found
     */
    @Override
    public Address findOne(final Object id) {
        final Query query = entityManager.createNativeQuery("select * from address where id = " + id, Address.class);
        Address address;
        try {
            address = (Address) query.getSingleResult();
        } catch (NoResultException e) {
            address = null;
        }

        return address;
    }

    @Override
    public Address save(final Address entity) {
        return entityManager.merge(entity);
    }

    @Override
    public Collection<Address> save(final Collection<Address> entities) {
        final List<Address> result = new ArrayList<Address>();

        if (entities == null) {
            return result;
        }

        for (Address entity : entities) {
            result.add(save(entity));
        }

        return result;
    }

    @Override
    public void delete(final Address entity) {
        final Address managedEntity = entityManager.merge(entity);
        entityManager.remove(managedEntity);
    }

    @Override
    public void deleteAll() {
        final Entity entity = getEntityClass().getAnnotation(Entity.class);
        final boolean hasName = (entity != null) && StringUtils.hasText(entity.name());
        final String entityName = hasName ? entity.name() : getEntityClass().getSimpleName();
        entityManager.createQuery(String.format(DELETE_ALL_QUERY, entityName)).executeUpdate();
        entityManager.clear();
    }

    private Class<Address> getEntityClass() {
        return Address.class;
    }

}
