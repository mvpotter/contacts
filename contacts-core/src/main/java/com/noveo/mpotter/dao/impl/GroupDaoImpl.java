/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 18.10.13
 * Time: 17:33
 */
package com.noveo.mpotter.dao.impl;

import com.noveo.mpotter.dao.GroupDao;
import com.noveo.mpotter.model.Group;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

/**
 * Group dao.
 */
@Repository
public class GroupDaoImpl extends AbstractBaseDaoImpl<Group> implements GroupDao {

    private static final String USERS = "users";
    private static final String ID = "id";

    @Override
    public List<Group> findAllWithUsersList(final int page, final int itemsOnPage) {
        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Group> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        final Root<Group> root = criteriaQuery.from(getEntityClass());
        root.fetch(USERS, JoinType.LEFT);
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).
                setFirstResult(page * itemsOnPage).setMaxResults(itemsOnPage).
                getResultList();
    }

    @Override
    public List<Group> findByIds(final List<Long> groupsIds) {
        if (groupsIds == null || groupsIds.isEmpty()) {
            return new LinkedList<Group>();
        }
        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Group> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        final Root<Group> root = criteriaQuery.from(getEntityClass());
        criteriaQuery.select(root);
        criteriaQuery.where(root.get(ID).in(groupsIds));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    protected Class<Group> getEntityClass() {
        return Group.class;
    }

}
