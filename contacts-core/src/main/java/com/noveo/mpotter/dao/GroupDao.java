/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 30.09.13
 * Time: 16:44
 */
package com.noveo.mpotter.dao;

import com.noveo.mpotter.model.Group;

import java.util.List;

/**
 * Group dao interface.
 */
public interface GroupDao extends BaseDao<Group> {

    List<Group> findAllWithUsersList(int page, int itemsOnPage);

    List<Group> findByIds(List<Long> groupsIds);
}
