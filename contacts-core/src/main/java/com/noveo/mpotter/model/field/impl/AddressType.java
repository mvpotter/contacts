/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 01.11.13
 * Time: 18:00
 */
package com.noveo.mpotter.model.field.impl;

/**
 * Address type.
 */
public enum AddressType {

    HOME(1),
    WORK(2),
    OTHER(3);

    private int id;

    AddressType(final int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public static AddressType parse(final int id) {
        AddressType addressType = null;
        for (AddressType item : AddressType.values()) {
            if (item.getId() == id) {
                addressType = item;
                break;
            }
        }
        return addressType;
    }

}
