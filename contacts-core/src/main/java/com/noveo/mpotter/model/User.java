/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 01.08.13
 * Time: 16:40
 */
package com.noveo.mpotter.model;

import com.noveo.mpotter.model.field.impl.AbstractField;
import com.noveo.mpotter.model.field.Field;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * User entity.
 */
@Entity
@Table(name = "user")
public class User extends IdEntity implements Serializable {

    @NotEmpty
    @Column(name = "login", nullable = false)
    private String login;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Avatar avatar;

    @ManyToMany (cascade = CascadeType.ALL)
    @JoinTable
    private Set<Group> groups = new LinkedHashSet<Group>();

    @OrderBy("order")
    @ManyToMany (cascade = CascadeType.ALL)
    private Set<Address> addresses = new LinkedHashSet<Address>();

    @OrderBy("order")
    @OneToMany (cascade = CascadeType.ALL, targetEntity = AbstractField.class)
    private Set<Field> additionalFields = new LinkedHashSet<Field>();

    public User() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public Avatar getAvatar() {
        return avatar;
    }

    public void setAvatar(final Avatar avatar) {
        this.avatar = avatar;
    }

    public Set<Group> getGroups() {
        return new HashSet<Group>(groups);
    }

    public void setGroups(final Set<Group> groups) {
        this.groups.clear();
        for (Group group: groups) {
            addGroup(group);
        }
    }

    public void addGroup(final Group group) {
        this.groups.add(group);
        if (!group.getUsers().contains(this)) {
            group.addUser(this);
        }
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(final Set<Address> addresses) {
        this.addresses.clear();
        for (Address address: addresses) {
            addAddress(address);
        }
    }

    public void addAddress(final Address address) {
        address.setOrder(this.addresses.size());
        this.addresses.add(address);
    }

    public Set<Field> getAdditionalFields() {
        return additionalFields;
    }

    public void setAdditionalFields(final Set<Field> additionalFields) {
        this.additionalFields.clear();
        for (Field field: additionalFields) {
            addAdditionalField(field);
        }
    }

    public void addAdditionalField(final Field field) {
        field.setOrder(this.additionalFields.size());
        this.additionalFields.add(field);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }

        boolean result = false;
        if (o instanceof User) {
            final User user = (User) o;
            result = super.equals(o)
                    && (getId() != null || login != null ? !login.equals(user.login) : user.login != null);
        } else if (o instanceof IdEntity) {
            result = super.equals(o);
        }

        return result;
    }

    @Override
    public int hashCode() {
        final int magic = 31;
        int result = super.hashCode();
        result = magic * result + (login != null ? login.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{"
                + "id=" + getId()
                + ", login='" + login + '\''
                + '}';
    }
}
