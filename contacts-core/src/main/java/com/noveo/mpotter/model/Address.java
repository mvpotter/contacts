/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 01.08.13
 * Time: 16:40
 */
package com.noveo.mpotter.model;

import com.noveo.mpotter.entitylistener.AddressEntityListener;
import com.noveo.mpotter.model.field.impl.AddressType;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Address entity.
 */
@Entity
@Table(name = "address")
@EntityListeners(AddressEntityListener.class)
public class Address extends IdEntity {

    @Column(name = "address_order")
    private Integer order;

    @Min(1)
    @Column(name = "type")
    private int type;

    @NotEmpty
    @Column(name = "city", nullable = false)
    private String city;

    @NotEmpty
    @Column(name = "street", nullable = false)
    private String street;

    @NotNull
    @Column(name = "last_updated", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date lastUpdated;

    @ManyToMany (mappedBy = "addresses", cascade = CascadeType.ALL)
    private Set<User> users = new HashSet<User>();

    public Address() {
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(final Integer order) {
        this.order = order;
    }

    public AddressType getType() {
        return AddressType.parse(type);
    }

    public void setType(final AddressType type) {
        this.type = type.getId();
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(final Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Set<User> getUsers() {
        return new HashSet<User>(users);
    }

    public void setUsers(final Set<User> users) {
        this.users.clear();
        for (User user: users) {
            addUser(user);
        }
    }

    public void addUser(final User user) {
        this.users.add(user);
        if (!user.getAddresses().contains(this)) {
            user.addAddress(this);
        }
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }

        boolean result = false;
        if (o instanceof Address) {
            final Address address = (Address) o;
            result = super.equals(o) && (getId() != null || allFieldsAreEqual(address));
        } else if (o instanceof IdEntity) {
            result = super.equals(o);
        }

        return result;
    }

    private boolean allFieldsAreEqual(final Address address) {
        if (city != null ? !city.equals(address.city) : address.city != null
            || street != null ? !street.equals(address.street) : address.street != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        final int magic = 31;
        if (getId() != null) {
            return super.hashCode();
        }
        int result = 0;
        result = magic * result + (city != null ? city.hashCode() : 0);
        result = magic * result + (street != null ? street.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Address{"
                + "id=" + getId()
                + ", city='" + city + '\''
                + ", street='" + street + '\''
                + ", type='" + type + '\''
                + '}';
    }
}
