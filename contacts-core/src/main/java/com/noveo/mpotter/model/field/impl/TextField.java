/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 05.08.13
 * Time: 18:29
 */
package com.noveo.mpotter.model.field.impl;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Text field.
 */
@Entity
@Table(name = "text_field")
public class TextField extends AbstractField {

    @NotEmpty
    @Column(name = "value", nullable = false)
    protected String value;

    public TextField() {
    }

    public TextField(final String name, final String value) {
        super(name);
        setValue(value);
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

}
