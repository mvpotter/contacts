/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 29/10/14
 * Time: 17:22
 */
package com.noveo.mpotter.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Specifies entity with id.
 */
@MappedSuperclass
public class IdEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || !(o instanceof IdEntity)) {
            return false;
        }

        final IdEntity idEntity = (IdEntity) o;

        if (id != null ? !id.equals(idEntity.id) : idEntity.id != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
