/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 05.08.13
 * Time: 18:29
 */
package com.noveo.mpotter.model.field.impl;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Link field.
 */
@Entity
@Table(name = "link_field")
public class LinkField extends TextField {

    public LinkField() {
    }

    public LinkField(final String name, final String value) {
        super(name, value);
    }

}
