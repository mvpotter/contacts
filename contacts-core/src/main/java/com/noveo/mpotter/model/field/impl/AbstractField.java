/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 05.08.13
 * Time: 18:20
 */
package com.noveo.mpotter.model.field.impl;

import com.noveo.mpotter.model.IdEntity;
import com.noveo.mpotter.model.User;
import com.noveo.mpotter.model.field.Field;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

/**
 * Base field class.
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractField extends IdEntity implements Field {

    private static final int NAME_LENGTH = 100;

    @Column(name = "field_order")
    private Integer order;

    @NotEmpty
    @Column(name = "name", nullable = false, length = NAME_LENGTH)
    private String name;

    @ManyToOne
    private User user;

    protected AbstractField() {
    }

    protected AbstractField(final String name) {
        setName(name);
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(final Integer order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setUser(final User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }

        boolean result = false;
        if (o instanceof AbstractField) {
            final Field field = (Field) o;
            result = super.equals(o)
                    && (getId() != null || name != null ? !name.equals(field.getName()) : field.getName() != null);
        } else if (o instanceof IdEntity) {
            result = super.equals(o);
        }

        return result;
    }

    @Override
    public int hashCode() {
        final int magic = 31;
        int result = super.hashCode();
        result = magic * result + (name != null ? name.hashCode() : 0);
        return result;
    }

}
