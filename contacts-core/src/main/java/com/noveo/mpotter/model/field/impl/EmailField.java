/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 05.08.13
 * Time: 18:29
 */
package com.noveo.mpotter.model.field.impl;

import org.hibernate.validator.constraints.Email;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Email field.
 */
@Entity
@Table(name = "email_field")
public class EmailField extends TextField {

    public EmailField() {
    }

    public EmailField(final String name, final String value) {
        super(name, value);
    }

    @Override
    @Email
    public String getValue() {
        return super.getValue();
    }

    @Override
    public void setValue(final String value) {
        super.setValue(value.toLowerCase());
    }

}
