/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 31.10.13
 * Time: 16:43
 */
package com.noveo.mpotter.model.field;

import com.noveo.mpotter.model.User;

/**
 * Field interface.
 */
public interface Field extends Identifiable, Nameable {

    void setOrder(Integer order);
    User getUser();

}
