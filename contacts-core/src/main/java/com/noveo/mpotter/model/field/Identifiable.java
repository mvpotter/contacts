/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 22.10.13
 * Time: 18:25
 */
package com.noveo.mpotter.model.field;

/**
 * Identifiable object interface.
 */
public interface Identifiable {

    Long getId();

}
