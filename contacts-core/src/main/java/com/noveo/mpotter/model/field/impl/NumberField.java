/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 05.08.13
 * Time: 18:29
 */
package com.noveo.mpotter.model.field.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Number field.
 */
@Entity
@Table(name = "number_field")
public class NumberField extends AbstractField {

    @NotNull
    @Column(name = "number", nullable = false)
    private Double number;

    public NumberField() {
    }

    public NumberField(final String name, final double value) {
        super(name);
        setNumber(value);
    }

    public double getNumber() {
        return number;
    }

    public void setNumber(final Double number) {
        this.number = number;
    }

}
