/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 01.08.13
 * Time: 16:38
 */
package com.noveo.mpotter.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * Group entity.
 */
@Entity
@Table(name = "contacts_group")
public class Group extends IdEntity {

    @NotEmpty
    @Column(name = "name", nullable = false)
    private String name;

    @ManyToMany (mappedBy = "groups", cascade = CascadeType.ALL)
    private Set<User> users = new HashSet<User>();

    public Group() {
    }

    public Group(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Set<User> getUsers() {
        return new HashSet<User>(users);
    }

    public void setUsers(final Set<User> users) {
        this.users.clear();
        for (User user: users) {
            addUser(user);
        }
    }

    public void addUser(final User user) {
        this.users.add(user);
        if (!user.getGroups().contains(this)) {
            user.addGroup(this);
        }
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        boolean result = false;
        if (o instanceof Group) {
            final Group group = (Group) o;
            result = super.equals(o)
                 && (getId() != null || name != null ? !name.equals(group.name) : group.name != null);
        } else if (o instanceof IdEntity) {
            result = super.equals(o);
        }

        return result;
    }

    @Override
    public int hashCode() {
        final int magic = 31;
        int result = super.hashCode();
        result = magic * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Group{"
                + "id=" + getId()
                + ", name='" + name + '\''
                + '}';
    }

}
