/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 22.10.13
 * Time: 18:25
 */
package com.noveo.mpotter.model.field;

/**
 * Nameable object interface.
 */
public interface Nameable {

    String getName();

}
