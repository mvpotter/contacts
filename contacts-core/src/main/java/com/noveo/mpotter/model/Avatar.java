/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 22.04.14
 * Time: 13:54
 */
package com.noveo.mpotter.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Avatar entity.
 */
@Entity
@Table(name = "avatar")
public class Avatar extends IdEntity {

    @Lob
    @Column(name = "image")
    @Basic(fetch = FetchType.LAZY)
    private byte[] image;

    @OneToOne (mappedBy = "avatar")
    private User user;

    public Avatar() {
    }

    public Avatar(final byte[] image) {
        this.image = image;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(final byte[] image) {
        this.image = image;
    }

    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }

}
