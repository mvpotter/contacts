/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 29.11.13
 * Time: 17:13
 */
package com.noveo.mpotter.service.impl;

import com.noveo.mpotter.dao.AddressDao;
import com.noveo.mpotter.model.Address;
import com.noveo.mpotter.service.AddressService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

/**
 * Service that manages addresses.
 */
@Service
public class AddressServiceImpl implements AddressService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddressServiceImpl.class);

    private final AddressDao addressDao;

    @Inject
    public AddressServiceImpl(final AddressDao addressDao) {
        this.addressDao = addressDao;
    }

    @Override
    public List<Address> getAddresses(final int page, final int itemsOnPage) {
        LOGGER.debug("getAddresses: page = {}, itemsOnPage = {}", page, itemsOnPage);
        final List<Address> addresses = addressDao.findAll(page, itemsOnPage);
        LOGGER.debug("getAddresses: addresses.size() = {}", addresses.size());
        return addresses;
    }

    @Override
    @Transactional
    public Address addAddress(final Address address) {
        LOGGER.debug("addAddress: {}" + address.toString());
        final Address savedAddress = addressDao.save(address);
        LOGGER.debug("addAddress: Address saved");
        return savedAddress;
    }

}
