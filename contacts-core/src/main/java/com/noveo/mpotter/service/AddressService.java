/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 29.11.13
 * Time: 16:55
 */
package com.noveo.mpotter.service;

import com.noveo.mpotter.model.Address;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service that manages addresses.
 */
@Service
public interface AddressService {

    List<Address> getAddresses(int page, int itemsOnPage);
    Address addAddress(Address address);

}
