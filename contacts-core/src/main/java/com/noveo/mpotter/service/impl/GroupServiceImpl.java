/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 29.11.13
 * Time: 17:13
 */
package com.noveo.mpotter.service.impl;

import com.noveo.mpotter.dao.GroupDao;
import com.noveo.mpotter.model.Group;
import com.noveo.mpotter.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

/**
 * Service that manages groups.
 */
@Service
public class GroupServiceImpl implements GroupService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GroupServiceImpl.class);

    private final GroupDao groupDao;

    @Inject
    public GroupServiceImpl(final GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    @Override
    public List<Group> getGroupsWithUsersList(final int page, final int itemsOnPage) {
        LOGGER.debug("getGroups: page = {}, itemsOnPage = {}", page, itemsOnPage);
        final List<Group> groups = groupDao.findAllWithUsersList(page, itemsOnPage);
        LOGGER.debug("getGroups: groups.size() = {}", groups.size());
        return groups;
    }

    @Override
    @Transactional
    public Group saveGroup(final Group group) {
        LOGGER.debug("saveGroup: {}" + group.toString());
        final Group savedGroup = groupDao.save(group);
        LOGGER.debug("saveGroup: Group saved");
        return savedGroup;
    }

}
