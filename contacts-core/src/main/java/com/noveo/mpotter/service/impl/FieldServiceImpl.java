/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 14/05/14
 * Time: 11:40
 */
package com.noveo.mpotter.service.impl;

import com.noveo.mpotter.dao.FieldDao;
import com.noveo.mpotter.model.field.Field;
import com.noveo.mpotter.model.field.impl.AbstractField;
import com.noveo.mpotter.service.FieldService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

/**
 * Service that manages fields.
 */
@Service
public class FieldServiceImpl implements FieldService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FieldServiceImpl.class);

    private FieldDao fieldDao;

    @Inject
    public FieldServiceImpl(final FieldDao fieldDao) {
        this.fieldDao = fieldDao;
    }

    @Override
    @Transactional
    public <T extends AbstractField> Field saveField(final T field) {
        LOGGER.debug("saveField: field = {}", field.toString());
        final Field savedField = fieldDao.save(field);
        LOGGER.debug("saveField: field saved");
        return savedField;
    }

}
