/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 20.01.14
 * Time: 15:41
 */
package com.noveo.mpotter.service.impl;

import com.noveo.mpotter.dao.AddressDao;
import com.noveo.mpotter.dao.FieldDao;
import com.noveo.mpotter.dao.GroupDao;
import com.noveo.mpotter.dao.UserDao;
import com.noveo.mpotter.model.Address;
import com.noveo.mpotter.model.Group;
import com.noveo.mpotter.model.User;
import com.noveo.mpotter.model.field.impl.AbstractField;
import com.noveo.mpotter.model.field.Field;
import com.noveo.mpotter.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Service that manages users.
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserDao userDao;
    private final GroupDao groupDao;
    private final AddressDao addressDao;
    private final FieldDao fieldDao;

    @Inject
    public UserServiceImpl(final UserDao userDao, final GroupDao groupDao,
                           final AddressDao addressDao, final FieldDao fieldDao) {
        this.userDao = userDao;
        this.groupDao = groupDao;
        this.addressDao = addressDao;
        this.fieldDao = fieldDao;
    }

    @Override
    @Transactional
    public User getUser(final Long id) {
        LOGGER.debug("getUser: id = {}", id);
        final User user = userDao.findOneEager(id);
        LOGGER.debug("getUser: user == null : {}", user == null);
        return user;
    }

    @Override
    public List<User> getUsers(final int page, final int itemsOnPage) {
        LOGGER.debug("getUsers: page = {}, itemsOnPage = {}", page, itemsOnPage);
        final List<User> users = userDao.findAll(page, itemsOnPage);
        LOGGER.debug("getUsers: users.size() = {}", users.size());
        return users;
    }

    @Override
    @Transactional
    public User saveUser(final User user) {
        LOGGER.debug("saveUser: {}" + user.toString());
        User savedUser = user;
        if (user.getId() != null) {
            savedUser = userDao.findOneEager(user.getId());
            if (savedUser == null) {
                LOGGER.warn("saveUser: user with id {} not found", user.getId());
                return null;
            }
            savedUser.setLogin(user.getLogin());
            savedUser.setAddresses(new LinkedHashSet<Address>(addressDao.save(user.getAddresses())));
            final Set<AbstractField> fields = new LinkedHashSet<AbstractField>();
            for (Field field: user.getAdditionalFields()) {
                fields.add((AbstractField) field);
            }
            savedUser.setAdditionalFields(new LinkedHashSet<Field>(fieldDao.save(fields)));
        }

        final List<Long> groupsIds = new LinkedList<Long>();
        for (Group group: user.getGroups()) {
            groupsIds.add(group.getId());
        }
        final Set<Group> groupsSet = new HashSet<Group>(groupDao.findByIds(groupsIds));
        savedUser.setGroups(groupsSet);
        savedUser = userDao.save(savedUser);
        LOGGER.debug("saveUser: User saved");
        return savedUser;
    }
}
