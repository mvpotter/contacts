/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 29.11.13
 * Time: 16:55
 */
package com.noveo.mpotter.service;

import com.noveo.mpotter.model.Group;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service that manages groups.
 */
@Service
public interface GroupService {

    List<Group> getGroupsWithUsersList(int page, int itemsOnPage);
    Group saveGroup(Group group);

}
