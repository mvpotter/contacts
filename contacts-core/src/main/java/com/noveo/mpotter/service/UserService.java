/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 29.11.13
 * Time: 16:55
 */
package com.noveo.mpotter.service;

import com.noveo.mpotter.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service that manages users.
 */
@Service
public interface UserService {

    User getUser(Long id);
    List<User> getUsers(int page, int itemsOnPage);
    User saveUser(User user);

}
