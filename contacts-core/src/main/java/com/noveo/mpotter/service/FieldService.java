/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 14/05/14
 * Time: 11:36
 */
package com.noveo.mpotter.service;

import com.noveo.mpotter.model.field.Field;
import com.noveo.mpotter.model.field.impl.AbstractField;
import org.springframework.stereotype.Service;

/**
 * Service that manages fields.
 */
@Service
public interface FieldService {

    <T extends AbstractField> Field saveField(T field);

}
