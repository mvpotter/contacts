/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 29/10/14
 * Time: 18:04
 */
package com.noveo.mpotter.transaction;

import javax.persistence.EntityManager;

/**
 * Responsible for transaction management.
 *
 * @param <T> type of result value
 */
public class TransactionExecutor<T> {

    private final EntityManager entityManager;

    public TransactionExecutor(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public T execute(final Transaction<T> transaction) {
        T result;
        try {
            entityManager.getTransaction().begin();
            result = transaction.perform();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            result = null;
        }

        return result;
    }

}

