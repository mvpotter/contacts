/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 29/10/14
 * Time: 18:04
 */
package com.noveo.mpotter.transaction;

/**
 * Transaction interface.
 *
 * @param <T> type of a return value
 */
public interface Transaction<T> {

    T perform();

}
