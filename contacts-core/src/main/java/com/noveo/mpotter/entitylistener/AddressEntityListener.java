/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 28.10.14
 * Time: 12:00
 */
package com.noveo.mpotter.entitylistener;

import com.noveo.mpotter.model.Address;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;

/**
 * Watches for changes in address entity.
 */
public class AddressEntityListener {

    @PreUpdate
    @PrePersist
    public void setLastUpdated(final Address address) {
        address.setLastUpdated(new Date());
    }

}
