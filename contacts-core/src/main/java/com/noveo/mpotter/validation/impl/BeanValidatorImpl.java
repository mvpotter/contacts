/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 15.05.14
 * Time: 16:47
 */
package com.noveo.mpotter.validation.impl;

import com.google.common.base.Joiner;
import com.noveo.mpotter.validation.BeanValidator;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.util.HashSet;
import java.util.Set;

/**
 * Class provides methods for entities validation.
 */
@Service
public class BeanValidatorImpl implements BeanValidator {

    private Validator validator;

    @Inject
    public BeanValidatorImpl(final Validator validator) {
        this.validator = validator;
    }

    public <T> void validate(final T entity) {
        final Set<ConstraintViolation<T>> constraintViolations = validator.validate(entity);
        if (!constraintViolations.isEmpty()) {
            final Set<String> violationMessages = new HashSet<String>();
            for (ConstraintViolation<T> constraintViolation : constraintViolations) {
                violationMessages.add(constraintViolation.getPropertyPath() + "="
                                    + constraintViolation.getMessageTemplate());
            }
            throw new ValidationException(Joiner.on("\n").join(violationMessages));
        }
    }

}
