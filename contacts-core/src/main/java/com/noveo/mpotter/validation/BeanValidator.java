/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 15.05.14
 * Time: 17:16
 */
package com.noveo.mpotter.validation;

/**
 * Interface for entities validation.
 */
public interface BeanValidator {

    <T> void validate(T entity);

}
