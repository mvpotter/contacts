/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 15.05.14
 * Time: 17:03
 */
package com.noveo.mpotter.validation;

import com.noveo.mpotter.model.Address;
import com.noveo.mpotter.model.field.impl.AddressType;
import org.junit.Test;

import javax.validation.ValidationException;

public class AddressValidationTest extends AbstractBaseValidationTest {

    @Test(expected = ValidationException.class)
    public void testCreateAddressWithNullType() {
        Address address = new Address();
        address.setCity("Novosibirsk");
        address.setStreet("Pirogova");
        beanValidator.validate(address);
    }

    @Test(expected = ValidationException.class)
    public void testCreateAddressWithEmptyCity() {
        Address address = new Address();
        address.setType(AddressType.HOME);
        address.setStreet("Pirogova");
        beanValidator.validate(address);
    }

    @Test(expected = ValidationException.class)
    public void testCreateAddressWithEmptyStreet() {
        Address address = new Address();
        address.setType(AddressType.HOME);
        address.setCity("Novosibirsk");
        beanValidator.validate(address);
    }

}
