/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 15.05.14
 * Time: 17:04
 */
package com.noveo.mpotter.validation;

import com.noveo.mpotter.AbstractBaseTest;

import javax.inject.Inject;

public abstract class AbstractBaseValidationTest extends AbstractBaseTest {

    @Inject
    protected BeanValidator beanValidator;

}
