/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 15.05.14
 * Time: 17:03
 */
package com.noveo.mpotter.validation;

import com.noveo.mpotter.model.Group;
import org.junit.Test;

import javax.validation.ValidationException;

public class GroupValidationTest extends AbstractBaseValidationTest {

    @Test(expected = ValidationException.class)
    public void testCreateGroupWithEmptyName() {
        Group group = new Group();
        beanValidator.validate(group);
    }

}
