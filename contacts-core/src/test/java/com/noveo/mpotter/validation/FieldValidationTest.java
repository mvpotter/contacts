/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 15.05.14
 * Time: 17:03
 */
package com.noveo.mpotter.validation;

import com.noveo.mpotter.model.field.impl.DateField;
import com.noveo.mpotter.model.field.impl.EmailField;
import com.noveo.mpotter.model.field.impl.TextField;
import org.junit.Test;

import javax.validation.ValidationException;

public class FieldValidationTest extends AbstractBaseValidationTest {

    @Test(expected = ValidationException.class)
     public void testCreateFieldWithEmptyName() {
        TextField textField = new TextField("", "some text");
        beanValidator.validate(textField);
    }

    @Test(expected = ValidationException.class)
    public void testCreateDateFieldWithNullDate() {
        DateField dateField = new DateField("Test date", null);
        beanValidator.validate(dateField);
    }

    @Test(expected = ValidationException.class)
    public void testCreateEmailFieldWithInvalidEmail() {
        EmailField emailField = new EmailField("Home", "mpotter");
        beanValidator.validate(emailField);
    }

}
