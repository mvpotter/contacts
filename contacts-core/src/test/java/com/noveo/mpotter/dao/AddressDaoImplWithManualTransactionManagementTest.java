/**
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 09.10.13
 * Time: 16:43
 */
package com.noveo.mpotter.dao;

import com.noveo.mpotter.AbstractBaseTest;
import com.noveo.mpotter.dao.impl.AddressDaoImplWithManualTransactionManagement;
import com.noveo.mpotter.model.Address;
import com.noveo.mpotter.model.field.impl.AddressType;
import com.noveo.mpotter.transaction.Transaction;
import com.noveo.mpotter.transaction.TransactionExecutor;
import com.noveo.mpotter.util.TestUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class AddressDaoImplWithManualTransactionManagementTest extends AbstractBaseTest {

    private AddressDao addressDao;

    private EntityManager entityManager;

    private TransactionExecutor<Address> transactionExecutor;

    @Before
    public void before() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("contactsUnit");
        entityManager = entityManagerFactory.createEntityManager();
        addressDao = new AddressDaoImplWithManualTransactionManagement(entityManager);
        transactionExecutor = new TransactionExecutor<Address>(entityManager);
    }

    @Test
    public void testFindOne() {
        final Address address = createTestAddress();
        Address entity = addressDao.findOne(address.getId());
        Assert.assertNotNull(entity);
        Assert.assertEquals(address.getType(), AddressType.OTHER);
        Assert.assertEquals(address.getCity(), entity.getCity());
        Assert.assertEquals(address.getStreet(), entity.getStreet());
    }

    @Test
    public void testUpdate() {
        final Address address = createTestAddress();
        String newStreet = RandomStringUtils.randomAlphanumeric(5);
        entityManager.detach(address);
        address.setStreet(newStreet);
        Address entity = addressDao.save(address);
        Assert.assertEquals(address.getStreet(), entity.getStreet());
    }

    @Test
    public void testTransaction() {
        final Address address = createTestAddress();
        final Address testAddress = TestUtils.createAddress();
        Address savedAddress = transactionExecutor.execute(new Transaction<Address>() {
            @Override
            public Address perform() {
                addressDao.delete(address);
                return addressDao.save(testAddress);
            }
        });
        Address entity = addressDao.findOne(savedAddress.getId());
        Assert.assertNotNull(entity);

        Address removedEntity = addressDao.findOne(address.getId());
        Assert.assertNull(removedEntity);
    }

    @Test
    public void transactionRollbackTest() {
        final Address address = createTestAddress();
        transactionExecutor.execute(new Transaction<Address>() {
            @Override
            public Address perform() {
                addressDao.delete(address);
                throw new IllegalStateException("Thrown to test transaction rollback");
            }
        });
        Address entity = addressDao.findOne(address.getId());
        Assert.assertNotNull(entity);
    }

    public void after() {
        addressDao.deleteAll();
    }

    private Address createTestAddress() {
        return transactionExecutor.execute(new Transaction<Address>() {
            @Override
            public Address perform() {
                return addressDao.save(TestUtils.createAddress());
            }
        });
    }

}
