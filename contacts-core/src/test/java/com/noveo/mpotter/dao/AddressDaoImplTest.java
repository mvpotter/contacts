/**
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 09.10.13
 * Time: 16:43
 */
package com.noveo.mpotter.dao;

import com.noveo.mpotter.AbstractBaseTest;
import com.noveo.mpotter.model.Address;
import com.noveo.mpotter.model.field.impl.AddressType;
import com.noveo.mpotter.util.TestUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

public class AddressDaoImplTest extends AbstractBaseTest {

    @Inject
    private AddressDao addressDao;

    private Address address = null;

    @Before
    public void before() {
        address = addressDao.save(TestUtils.createAddress());
    }

    @Test
    @Transactional
    public void testFindOne() {
        Address entity = addressDao.findOne(address.getId());
        Assert.assertNotNull(entity);
        Assert.assertEquals(address.getType(), AddressType.OTHER);
        Assert.assertEquals(address.getCity(), entity.getCity());
        Assert.assertEquals(address.getStreet(), entity.getStreet());
    }

    @Test
    @Transactional
    public void testUpdate() {
        String newStreet = RandomStringUtils.randomAlphanumeric(5);
        address.setStreet(newStreet);
        addressDao.save(address);
        Address entity = addressDao.findOne(address.getId());
        Assert.assertEquals(address.getStreet(), entity.getStreet());
    }

    @After
    public void after() {
        addressDao.delete(address);
    }

}
