/**
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 18.10.13
 * Time: 17:41
 */
package com.noveo.mpotter.dao;

import com.noveo.mpotter.AbstractBaseTest;
import com.noveo.mpotter.model.Address;
import com.noveo.mpotter.model.Group;
import com.noveo.mpotter.model.User;
import com.noveo.mpotter.model.field.Field;
import com.noveo.mpotter.util.TestUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DaoImplTest extends AbstractBaseTest {

    @Inject
    private GroupDao groupDao;
    @Inject
    private AddressDao addressDao;
    @Inject
    private FieldDao fieldDao;
    @Inject
    private UserDao userDao;

    private List<User> users;
    private List<Group> groups;
    private Set<Field> fields;
    private Set<Address> addresses;

    @Before
    public void before() {
        users = Arrays.asList(userDao.save(TestUtils.createUser()), userDao.save(TestUtils.createUser()));

        groups = Arrays.asList(groupDao.save(TestUtils.createGroup()), groupDao.save(TestUtils.createGroup()));
        addresses = new HashSet<Address>(Arrays.asList(new Address[]{addressDao.save(TestUtils.createAddress()),
                addressDao.save(TestUtils.createAddress())}));

        fields = new HashSet<Field>(Arrays.asList(new Field[]{
                fieldDao.save(TestUtils.createEmailField()),
                fieldDao.save(TestUtils.createDateField()),
                fieldDao.save(TestUtils.createEmailField()),
                fieldDao.save(TestUtils.createLinkField()),
                fieldDao.save(TestUtils.createNumberField()),
                fieldDao.save(TestUtils.createNumberField()),
                fieldDao.save(TestUtils.createTextField()),
                fieldDao.save(TestUtils.createEmailField())}));

        users.get(0).setGroups(new HashSet<Group>(groups));
        users.get(0).setAddresses(addresses);
        users.get(0).setAdditionalFields(fields);

        users.get(1).setGroups(new HashSet<Group>(Arrays.asList(new Group[]{groups.iterator().next()})));

        userDao.save(users);
    }

    @Test
    @Transactional
    public void testFindOne() {
        User user = userDao.findOne(users.get(0).getId());
        Assert.assertTrue(addresses.containsAll(user.getAddresses()) && user.getAddresses().containsAll(addresses));
        Assert.assertTrue(groups.containsAll(user.getGroups()) && user.getGroups().containsAll(groups));
        Assert.assertTrue(fields.containsAll(user.getAdditionalFields()) && user.getAdditionalFields().containsAll(fields));

        Assert.assertTrue(groups.get(0).getUsers().containsAll(users));
        Assert.assertTrue(groups.get(1).getUsers().contains(users.get(0)) && !groups.get(1).getUsers().contains(users.get(1)));
    }

}
