/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 12.05.14
 * Time: 15:47
 */
package com.noveo.mpotter.dao;

import com.noveo.mpotter.AbstractBaseTest;
import com.noveo.mpotter.model.Group;
import com.noveo.mpotter.util.TestUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

public class GroupDaoImplTest extends AbstractBaseTest {

    @Inject
    private GroupDao groupDao;

    private Group group = null;

    @Before
    @Transactional
    public void before() {
        group = TestUtils.createGroup();
        group.addUser(TestUtils.createUser());
        group.addUser(TestUtils.createUser());
        group = groupDao.save(group);
    }

    @Test
    @Transactional
    public void testFindOne() {
        Group entity = groupDao.findOne(group.getId());
        Assert.assertNotNull(entity);
        Assert.assertEquals(group.getName(), entity.getName());
        Assert.assertEquals(group.getUsers().size(), entity.getUsers().size());
    }

    @Test
    @Transactional
    public void testFindAllWithUsersList() {
        List<Group> groups = groupDao.findAllWithUsersList(0, ITEMS_ON_PAGE);
        Assert.assertEquals(1, groups.size());
        Group entity = groups.get(0);
        Assert.assertEquals(group.getUsers().size(), entity.getUsers().size());
    }

    @After
    public void after() {
        groupDao.delete(group);
    }

}
