/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 13.05.14
 * Time: 16:31
 */
package com.noveo.mpotter.dao;

import com.noveo.mpotter.AbstractBaseTest;
import com.noveo.mpotter.model.field.impl.AbstractField;
import com.noveo.mpotter.model.field.impl.DateField;
import com.noveo.mpotter.model.field.impl.EmailField;
import com.noveo.mpotter.model.field.impl.LinkField;
import com.noveo.mpotter.model.field.impl.NumberField;
import com.noveo.mpotter.model.field.impl.TextField;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class FieldDaoTest extends AbstractBaseTest {

    @Inject
    private FieldDao fieldDao;

    private List<AbstractField> fields;

    @Before
    @Transactional
    public void before() {
        fields = Arrays.asList(new DateField("Birthday", new Date()),
                               new EmailField("Work", "mpotter@noveogroup.com"),
                               new LinkField("Company", "http://noveogroup.com"),
                               new NumberField("Room", 11),
                               new TextField("Position", "Java developer"));
        fields = new LinkedList<AbstractField>(fieldDao.save(fields));
    }

    @Test
    @Transactional
    public void testFindAll() {
        List<AbstractField> entities = fieldDao.findAll(0, ITEMS_ON_PAGE);
        Assert.assertNotNull(entities);
        Assert.assertEquals(fields.size(), entities.size());
    }

    @After
    @Transactional
    public void after() {
        fieldDao.deleteAll();
    }

}
