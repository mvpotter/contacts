/**
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 16.10.13
 * Time: 17:09
 */
package com.noveo.mpotter.util;

import com.noveo.mpotter.model.Address;
import com.noveo.mpotter.model.Avatar;
import com.noveo.mpotter.model.field.impl.AddressType;
import com.noveo.mpotter.model.Group;
import com.noveo.mpotter.model.User;
import com.noveo.mpotter.model.field.impl.DateField;
import com.noveo.mpotter.model.field.impl.EmailField;
import com.noveo.mpotter.model.field.impl.LinkField;
import com.noveo.mpotter.model.field.impl.NumberField;
import com.noveo.mpotter.model.field.impl.TextField;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Random;


public class TestUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestUtils.class);

    private static final int TEST_GENERATED_STRING_LENGTH = 10;

    private static final String EMAIL_FORMAT = "%s@%s.%s";

    private TestUtils () {

    }

    public static Address createAddress() {
        Address address = new Address();

        address.setOrder(RandomUtils.nextInt());
        address.setType(AddressType.OTHER);
        address.setStreet(RandomStringUtils.randomAlphanumeric(TEST_GENERATED_STRING_LENGTH));
        address.setCity(RandomStringUtils.randomAlphanumeric(TEST_GENERATED_STRING_LENGTH));

        return address;
    }

    public static Group createGroup() {
        Group group = new Group();

        group.setName(RandomStringUtils.randomAlphanumeric(TEST_GENERATED_STRING_LENGTH));

        return group;
    }

    public static DateField createDateField() {
        DateField dateField = new DateField();

        dateField.setName(RandomStringUtils.randomAlphanumeric(TEST_GENERATED_STRING_LENGTH));
        dateField.setDate(new Date());

        return dateField;
    }

    public static EmailField createEmailField() {
        EmailField emailField = new EmailField();

        emailField.setName(RandomStringUtils.randomAlphanumeric(TEST_GENERATED_STRING_LENGTH));
        emailField.setValue(String.format(EMAIL_FORMAT,
                                          RandomStringUtils.randomAlphanumeric(5),
                                          RandomStringUtils.randomAlphanumeric(5),
                                          RandomStringUtils.randomAlphabetic(2)));

        return emailField;
    }

    public static LinkField createLinkField() {
        LinkField linkField = new LinkField();

        linkField.setName(RandomStringUtils.randomAlphabetic(TEST_GENERATED_STRING_LENGTH));
        linkField.setValue(RandomStringUtils.randomAlphabetic(TEST_GENERATED_STRING_LENGTH));

        return linkField;
    }

    public static NumberField createNumberField() {
        NumberField numberField = new NumberField();

        numberField.setName(RandomStringUtils.randomAlphabetic(TEST_GENERATED_STRING_LENGTH));
        numberField.setNumber((new Random()).nextDouble());

        return numberField;
    }

    public static TextField createTextField() {
        TextField textField = new TextField();

        textField.setName(RandomStringUtils.randomAlphabetic(TEST_GENERATED_STRING_LENGTH));
        textField.setValue(RandomStringUtils.randomAlphabetic(TEST_GENERATED_STRING_LENGTH));

        return textField;
    }

    public static User createUser() {
        User user = new User();

        user.setLogin(RandomStringUtils.randomAlphabetic(TEST_GENERATED_STRING_LENGTH));
        user.setAvatar(new Avatar(convertImageToByteArray()));

        return user;
    }

    private static byte[] convertImageToByteArray() {
        try {
            final byte[] imageInBytes;
            final InputStream inputStream = TestUtils.class.getResourceAsStream("/images/empty_avatar.jpg");

            final BufferedImage originalImage = ImageIO.read(inputStream);

            // convert BufferedImage to byte array
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(originalImage, "jpg", baos);
            baos.flush();
            imageInBytes = baos.toByteArray();
            baos.close();
            return imageInBytes;
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }


}
