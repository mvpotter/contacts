/**
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 18.10.13
 * Time: 17:43
 */
package com.noveo.mpotter;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/com/noveo/mpotter/contacts-core-application-context-test.xml"})
public abstract class AbstractBaseTest {

    public static final int ITEMS_ON_PAGE = 100;

}
