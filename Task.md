## Contacts

The application should provide SOAP interface to implement CRUD operations on contacts.
Moreover, it is necessary to implement a simple web client for interacting with service.
Build system is Maven.

#### Model

**Contact**

- ***Name*** - contact name
- ***Avatar*** - contact avatar (should be lazy loaded)
- ***Groups*** - set of groups contact takes part in
- ***Addresses*** - contact can have a number of addresses specified by type (home, work, other)
- ***Fields*** - contact can have a number of additional fields, fields could have the following types:
  - date
  - email
  - link
  - number
  - text