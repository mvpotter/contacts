<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <c:choose>
            <c:when test="${user.login == null || user.login == \"\"}">
                <title>New user</title>
            </c:when>
            <c:otherwise>
                <title>${user.login}</title>
            </c:otherwise>
        </c:choose>
    </head>
    <body>
        <div class="col-xs-12">
            <form method="post" action="<%= request.getContextPath() %>/groups/${group.id}/saveContact" role="form">
                <div class="row section">
                    <div class="col-xs-2">
                        <img alt="${user.login}" title="${user.login}" class="avatar"
                             src="data:image/jpg;base64,<c:out value="${avatar}" />" />
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <form:hidden path="user.id" />
                            <form:label path="user.login">Login</form:label>
                            <form:input path="user.login" class="form-control" />
                            <form:errors path="user.login" cssClass="error" />
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <form:label path="user.groups">Groups</form:label>
                            <p><form:errors path="user.groups" cssClass="error" /></p>
                            <c:forEach var="group" items="${groups}" varStatus="status">
                                <div id="groups_container" class="checkbox">
                                    <label>
                                        <form:checkbox path="user.groups" value="${group}" />${group.name}
                                    </label>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
                <div class="row section">
                    <div class="col-xs-12">
                        <div id="addresses_container" class="form-group">
                            <form:label path="user.addresses">Addresses</form:label>
                            <c:forEach items="${user.addresses}" varStatus="status">
                                <div class="row address_container">
                                    <div class="col-xs-2">
                                        <form:select path="user.addresses[${status.index}].type" items="${addressTypes}"
                                                     class="form-control address_type" />
                                    </div>
                                    <div class="col-xs-2">
                                        <form:input path="user.addresses[${status.index}].city"
                                                    class="form-control address_city" placeholder="City" />
                                        <form:errors path="user.addresses[${status.index}].city" cssClass="error" />
                                    </div>
                                    <div class="col-xs-2">
                                        <form:input path="user.addresses[${status.index}].street"
                                                    class="form-control address_street" placeholder="Street" />
                                        <form:errors path="user.addresses[${status.index}].street" cssClass="error" />
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <a id="add_address" href="#">Add address</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row section">
                    <div class="col-xs-12 col-sm-6">
                        <div id="fields_container" class="form-group">
                            <form:label path="user.fields">Other</form:label>
                            <c:forEach items="${user.fields}" var="field" varStatus="status">
                                <div class="row field_container">
                                    <div class="col-xs-8 col-sm-4">
                                        <form:select path="user.fields[${status.index}].type" items="${fieldTypes}"
                                                     class="form-control field_type" />
                                    </div>
                                    <div class="col-xs-8 col-sm-4">
                                        <form:input path="user.fields[${status.index}].name"
                                                    class="form-control field_name"
                                                    placeholder="Name"/>
                                        <form:errors path="user.fields[${status.index}].name" cssClass="error" />
                                    </div>
                                    <div class="col-xs-8 col-sm-4">
                                        <form:input path="user.fields[${status.index}].value"
                                                    class="form-control field_value"
                                                    placeholder="Value"/>
                                        <form:errors path="user.fields[${status.index}].value" cssClass="error" />
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <a id="add_field" href="#">Add field</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <input id="submit" class="pull-right" type="submit" value="Save" />
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>