<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <c:choose>
            <c:when test="${group.name == null || group.name == \"\"}">
                <title>New group</title>
            </c:when>
            <c:otherwise>
                <title>${group.name}</title>
            </c:otherwise>
        </c:choose>
    </head>
    <body>
        <div class="col-xs-12">
            <form method="post" action="<%= request.getContextPath() %>/groups/save" role="form">
                <div class="row section">
                    <div class="col-xs-2">
                        <div class="form-group">
                            <form:hidden path="group.id" />
                            <form:label path="group.name">Name</form:label>
                            <form:input path="group.name" class="form-control" />
                            <form:errors path="group.name" cssClass="error" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-2">
                        <button id="button" class="btn btn-default pull-right" type="submit">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
