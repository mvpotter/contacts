<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <c:if test="${group == null}">
            <c:set var="group" value="${groups[0]}" />
        </c:if>
        <title>${group.name}</title>
    </head>
    <body>
        <div class="col-xs-12">
            <c:if test="${not empty group.users}">
                <a type="submit" href="<%= request.getContextPath() %>/groups/${group.id}/users/add">Add contact</a>
            </c:if>
            <div class="row section">
                <div class="col-xs-12">
                    <c:choose>
                        <c:when test="${not empty group.users}">
                            <ul class="list-unstyled">
                                <c:forEach items="${group.users}" var="user">
                                    <li>
                                        <a href="<%= request.getContextPath() %>/groups/${group.id}/users/${user.id}">${user.login}</a>
                                    </li>
                                </c:forEach>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            No contacts yet. It is never late to <a href="<%= request.getContextPath() %>/groups/${group.id}/users/add">add one</a>.
                        </c:otherwise>
                    </c:choose>
                </div>
           </div>
        </div>
    </body>
</html>