<%--
  Created by IntelliJ IDEA.
  User: mpotter
  Date: 04.07.14
  Time: 14:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/css/style.min.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/libs/bootstrap-3.2.0/css/bootstrap.min.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/libs/bootstrap-3.2.0/css/bootstrap-theme.min.css" media="screen"/>

    <script type="text/javascript" src="<%= request.getContextPath() %>/resources/libs/jquery-1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/resources/libs/bootstrap-3.2.0/js/bootstrap.min.js"></script>

    <title>404 Page not found - Contacts</title>
</head>
<body>
    <div class="container">
        <div class="row vertical-center-row">
            <div class="col-xs-12">
                <div class="row ">
                    <div class="col-xs-4 col-xs-offset-4">
                        <h3>404 Not found</h3>
                        It seems that page you are looking for is not found.
                        Please, start from the very <a href="<%= request.getContextPath() %>">beginning</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
