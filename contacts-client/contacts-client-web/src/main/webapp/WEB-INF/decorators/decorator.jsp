<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/css/style.min.css" media="screen"/>
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/libs/bootstrap-3.2.0/css/bootstrap.min.css" media="screen"/>
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/resources/libs/bootstrap-3.2.0/css/bootstrap-theme.min.css" media="screen"/>

        <script type="text/javascript" src="<%= request.getContextPath() %>/resources/libs/jquery-1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/resources/libs/bootstrap-3.2.0/js/bootstrap.min.js"></script>

        <script type="text/javascript" src="<%= request.getContextPath() %>/resources/js/contacts.min.js"></script>

        <title><sitemesh:write property="title"/> - Contacts</title>
    </head>
    <body>
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <li class="sidebar-brand">
                        <a href="<%= request.getContextPath() %>">Contacts</a>
                    </li>
                    <c:if test="${group == null}">
                        <c:set var="group" value="${groups[0]}" />
                    </c:if>
                    <li>
                        <a id="add_group_button" type="submit" class="btn btn-success"
                           href="<%= request.getContextPath() %>/groups/add">Add group</a>
                    </li>
                    <c:forEach items="${groups}" var="groupItem">
                        <li <c:if test="${group.id == groupItem.id}">class="active"</c:if>>
                            <a href="<%= request.getContextPath() %>/groups/${groupItem.id}">${groupItem.name} (${fn:length(groupItem.users)})</a>
                        </li>
                    </c:forEach>
                </ul>
            </div>

            <!-- Page content -->
            <div id="page-content-wrapper">
                <div class="content-header">
                    <h1>
                        <a id="menu-toggle" href="#" class="btn btn-default"><i class="icon-reorder"></i></a>
                        <sitemesh:write property="title"/>
                    </h1>
                </div>
                <!-- Keep all page content within the page-content inset div! -->
                <div class="page-content inset">
                    <div class="row">
                        <sitemesh:write property="body"/>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>