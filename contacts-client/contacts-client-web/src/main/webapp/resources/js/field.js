/*
 * Created by mpotter on 03.07.14.
 */
$(document).ready(function() {

	$('a#add_field').click(function(event) {
		var field_container = $('div.field_container:last');
		var index = $('div.field_container').index(field_container) + 1;
		field_container = field_container.clone();
		field_container.find('.field_type').attr({'id': 'fields' + index + '.type',
												  'name': 'fields[' + index + '].type'});
		field_container.find('.field_name').attr({'id': 'fields' + index + '.name',
												   'name': 'fields[' + index + '].name',
												   'value': ''});
		field_container.find('.field_value').attr({'id': 'fields' + index + '.value',
												   'name': 'fields[' + index + '].value',
												   'value': ''});
		// Remove error messages if exist
		field_container.find('.error').remove();
		field_container.appendTo('div#fields_container');
		event.preventDefault();
	});

});
