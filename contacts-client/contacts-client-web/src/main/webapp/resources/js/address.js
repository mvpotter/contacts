/*
 * Created by mpotter on 03.07.14.
 */
$(document).ready(function() {

	$('a#add_address').click(function(event) {
		var address_container = $('div.address_container:last');
		var index = $('div.address_container').index(address_container) + 1;
		address_container = address_container.clone();
		address_container.find('.address_type').attr({'id': 'addresses' + index + '.type',
												  	  'name': 'addresses[' + index + '].type'});
		address_container.find('.address_city').attr({'id': 'addresses' + index + '.city',
													  'name': 'addresses[' + index + '].city',
													  'value': ''});
		address_container.find('.address_street').attr({'id': 'addresses' + index + '.street',
														'name': 'addresses[' + index + '].street',
													    'value': ''});
		// Remove error messages if exist
		address_container.find('.error').remove();
		address_container.appendTo('div#addresses_container');
		event.preventDefault();
	});

});
