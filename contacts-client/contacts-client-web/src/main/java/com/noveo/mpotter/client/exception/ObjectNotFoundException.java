/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 04.07.14
 * Time: 12:37
 */
package com.noveo.mpotter.client.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception that should be thrown in case if entity is not found by controller.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such object")
public class ObjectNotFoundException extends RuntimeException {
}
