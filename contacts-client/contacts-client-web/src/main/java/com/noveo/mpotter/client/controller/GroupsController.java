/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 24.06.14
 * Time: 13:53
 */
package com.noveo.mpotter.client.controller;

import com.noveo.mpotter.client.model.Group;
import com.noveo.mpotter.soap.client.GroupClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import javax.validation.Valid;

/**
 * Tests Spring MVC controller.
 */
@Controller
@RequestMapping("/")
public class GroupsController {

    public static final String GROUP = "group";
    private static final String USERS_LIST_PAGE = "contacts/users_list";
    private static final String ADD_GROUP_PAGE = "contacts/add_group";

    private final GroupClient groupClient;

    @Inject
    public GroupsController(final GroupClient groupClient) {
        this.groupClient = groupClient;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String showGroups() {
        return USERS_LIST_PAGE;
    }

    @RequestMapping(value = "groups/{id}", method = RequestMethod.GET)
    public String showGroup(@PathVariable final long id, final ModelMap model) {
        model.addAttribute(GROUP, id);
        return USERS_LIST_PAGE;
    }

    @RequestMapping(value = "groups/add", method = RequestMethod.GET)
    public String addGroup(final ModelMap model) {
        model.addAttribute(GROUP, new Group());
        return ADD_GROUP_PAGE;
    }

    @RequestMapping(value = "groups/save", method = RequestMethod.POST)
    public String addGroup(@Valid @ModelAttribute(GROUP) final Group group, final BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ADD_GROUP_PAGE;
        }
        final long groupId = groupClient.saveGroup(group);
        return "redirect:/groups/" + groupId;
    }

}
