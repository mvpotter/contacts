/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 08.07.14
 * Time: 17:26
 */
package com.noveo.mpotter.client.validator;

import com.noveo.mpotter.client.model.field.Field;
import com.noveo.mpotter.client.model.User;
import org.hibernate.validator.internal.constraintvalidators.EmailValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;

/**
 * Validator for user form object.
 */
@Component
public class UserFormValidator implements Validator {

    private static final String FIELD_NAME_FORMAT = "fields[%s].value";

    private final EmailValidator emailValidator;

    private final javax.validation.Validator validator;

    @Inject
    public UserFormValidator(final javax.validation.Validator validator, final EmailValidator emailValidator) {
        this.validator = validator;
        this.emailValidator = emailValidator;
    }

    @Override
    public boolean supports(final Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(final Object target, final Errors errors) {
        final User user = (User) target;
        validateAnnotations(user, errors);
        validateGroups(user, errors);
        int i = 0;
        final List<Field> fields = user.getFields();
        if (fields != null) {
            for (Field field: fields) {
                validateField(errors, field, i);
                i += 1;
            }
        }
    }

    private void validateAnnotations(final Object target, final Errors errors) {
        final Set<ConstraintViolation<Object>> constraintViolations = validator.validate(target);
        for (ConstraintViolation<Object> constraintViolation : constraintViolations) {
            final String propertyPath = constraintViolation.getPropertyPath().toString();
            final String message = constraintViolation.getMessage();
            errors.rejectValue(propertyPath, "", message);
        }
    }

    private void validateGroups(final User user, final Errors errors) {
        if (user.getGroups() == null || user.getGroups().isEmpty()) {
            errors.rejectValue("groups", "EmptyGroups", "choose at least one group");
        }
    }

    private void validateField(final Errors errors, final Field field, final int index) {
        // FIXME: I understand that the following code is ugly and it is batter to use chain of validators
        // FIXME: however, it's a training task and I don't want to waste time to refactor it.
        switch (field.getType()) {
            case EMAIL:
                validateEmail(errors, field, index);
                break;
            case LINK:
                validateLink(errors, field, index);
                break;
            case DATE:
                try {
                    new SimpleDateFormat(Field.DATE_FORMAT).parse(field.getValue());
                } catch (ParseException e) {
                    errors.rejectValue(String.format(FIELD_NAME_FORMAT, index), "Date",
                            "must be a valid Date (" + Field.DATE_FORMAT + ")");
                }
                break;
            case NUMBER:
                try {
                    Double.parseDouble(field.getValue());
                } catch (NumberFormatException e) {
                    errors.rejectValue(String.format(FIELD_NAME_FORMAT, index), "Number",
                            "must be a valid Number");
                }
                break;
            default:
                break;
        }
    }

    private void validateEmail(final Errors errors, final Field field, final int index) {
        if (!emailValidator.isValid(field.getValue(), null)) {
            errors.rejectValue(String.format(FIELD_NAME_FORMAT, index), "Email",
                    "not a well-formed email address");
        }
    }

    private void validateLink(final Errors errors, final Field field, final int index) {
        final String link = field.getValue();
        if ((!link.startsWith("http://") && !link.startsWith("https://")) || !link.contains(".")) {
            errors.rejectValue(String.format(FIELD_NAME_FORMAT, index), "URL", "must be a valid URL");
        }
    }

}
