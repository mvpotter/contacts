/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 24.06.14
 * Time: 13:53
 */
package com.noveo.mpotter.client.controller;

import com.noveo.mpotter.client.editor.GroupPropertyEditor;
import com.noveo.mpotter.client.exception.ObjectNotFoundException;
import com.noveo.mpotter.client.model.field.Field;
import com.noveo.mpotter.client.helper.UserHelper;
import com.noveo.mpotter.client.model.Address;
import com.noveo.mpotter.client.model.Group;
import com.noveo.mpotter.client.model.User;
import com.noveo.mpotter.soap.client.UserClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Arrays;

/**
 * Tests Spring MVC controller.
 */
@Controller
public class UsersController {

    private static final String USER = "user";
    private static final String AVATAR = "avatar";
    private static final String USER_DETAILS_PAGE = "contacts/user_details";

    private final UserClient userClient;
    private final Validator userFormValidator;
    private final UserHelper userHelper;

    private final GroupPropertyEditor groupPropertyEditor;

    @Inject
    public UsersController(final UserClient userClient, final Validator userFormValidator,
                           final UserHelper userHelper, final GroupPropertyEditor groupPropertyEditor) {
        this.userClient = userClient;
        this.userFormValidator = userFormValidator;
        this.userHelper = userHelper;
        this.groupPropertyEditor = groupPropertyEditor;
    }

    @InitBinder
    public void initBinder(final WebDataBinder binder) {
        binder.registerCustomEditor(Group.class, groupPropertyEditor);
    }

    @RequestMapping(value = "/groups/{groupId}/users/add", method = RequestMethod.GET)
    public String addUser(@PathVariable final long groupId, final ModelMap model) throws IOException {
        model.addAttribute(GroupsController.GROUP, groupId);

        final User user = new User();
        final Group group = new Group();
        group.setId(groupId);
        user.setGroups(Arrays.asList(group));
        user.setAddresses(Arrays.asList(new Address()));
        user.setFields(Arrays.asList(new Field()));
        model.addAttribute(USER, user);
        model.addAttribute(AVATAR, userHelper.getAvatar(user));

        userHelper.setAddressTypes(model);
        userHelper.setFieldTypes(model);

        return USER_DETAILS_PAGE;
    }

    @RequestMapping(value = "groups/{groupId}/users/{userId}", method = RequestMethod.GET)
    public String showUser(@PathVariable final long groupId,
                           @PathVariable final long userId, final ModelMap model) throws IOException {
        model.addAttribute(GroupsController.GROUP, groupId);

        final User user = userClient.getUser(userId);
        if (user == null) {
            throw new ObjectNotFoundException();
        }

        model.addAttribute(USER, user);
        model.addAttribute(AVATAR, userHelper.getAvatar(user));

        userHelper.reviseUserFields(user);
        userHelper.setAddressTypes(model);
        userHelper.setFieldTypes(model);

        return USER_DETAILS_PAGE;
    }

    @RequestMapping(value = "/groups/{groupId}/saveContact", method = RequestMethod.POST)
    public String saveContact(@PathVariable final long groupId, @ModelAttribute(USER) final User userForm,
                              final BindingResult bindingResult, final ModelMap model) throws IOException {
        model.addAttribute(GroupsController.GROUP, groupId);

        userHelper.removeEmptyFields(userForm);
        userFormValidator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            final User retrievedUser = userClient.getUser(userForm.getId());
            model.addAttribute(AVATAR, userHelper.getAvatar(retrievedUser));
            userHelper.reviseUserFields(userForm);
            userHelper.setAddressTypes(model);
            userHelper.setFieldTypes(model);

            return USER_DETAILS_PAGE;
        }
        final long userId = userClient.saveUser(userForm);
        return "redirect:/groups/" + userHelper.getSelectedGroupId(userForm, groupId) + "/users/" + userId;
    }

}
