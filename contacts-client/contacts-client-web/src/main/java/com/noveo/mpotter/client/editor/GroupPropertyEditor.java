/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 03.07.14
 * Time: 12:16
 */
package com.noveo.mpotter.client.editor;

import com.noveo.mpotter.client.model.Group;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;

/**
 * Editor for converting groups list property.
 */
@Component
public class GroupPropertyEditor extends PropertyEditorSupport {

    @Override
    public void setAsText(final String text) {
        if (text.isEmpty()) {
            return;
        }
        final Group group = new Group();
        group.setId(Long.parseLong(text));
        setValue(group);
    }

    @Override
    public String getAsText() {
        if (getValue() == null) {
            return "";
        }
        final Group group = (Group) getValue();
        return String.valueOf(group.getId());
    }

}
