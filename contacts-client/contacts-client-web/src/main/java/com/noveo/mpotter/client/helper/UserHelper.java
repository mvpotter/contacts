/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 09.07.14
 * Time: 15:57
 */
package com.noveo.mpotter.client.helper;

import com.google.common.io.ByteStreams;
import com.noveo.mpotter.client.model.field.Field;
import com.noveo.mpotter.client.model.Address;
import com.noveo.mpotter.client.model.Group;
import com.noveo.mpotter.client.model.User;
import com.noveo.mpotter.client.model.field.FieldType;
import org.apache.geronimo.mail.util.Base64;
import org.springframework.ui.ModelMap;

import javax.inject.Named;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * File with helper methods for processing User fields.
 */
@Named
public class UserHelper {

    private static final String ADDRESS_TYPES = "addressTypes";
    private static final String FIELD_TYPES = "fieldTypes";

    public void removeEmptyFields(final User user) {
        final Iterator<Address> addressIterator = user.getAddresses().iterator();
        Address address;
        while (addressIterator.hasNext()) {
            address = addressIterator.next();
            if (address.getCity().isEmpty() && address.getStreet().isEmpty()) {
                addressIterator.remove();
            }
        }
        final Iterator<Field> fieldIterator = user.getFields().iterator();
        Field field;
        while (fieldIterator.hasNext()) {
            field = fieldIterator.next();
            if (field.getName().isEmpty() && field.getValue().isEmpty()) {
                fieldIterator.remove();
            }
        }
    }

    public void reviseUserFields(final User user) {
        if (user.getAddresses() == null || user.getAddresses().isEmpty()) {
            user.setAddresses(Arrays.asList(new Address()));
        }
        if (user.getFields() == null || user.getFields().isEmpty()) {
            user.setFields(Arrays.asList(new Field()));
        }
    }

    public void setAddressTypes(final ModelMap model) {
        final Map<String, String> addressTypes = new HashMap<String, String>();
        for (AddressType addressType: AddressType.values()) {
            addressTypes.put(addressType.name(), addressType.name().toLowerCase());
        }

        model.addAttribute(ADDRESS_TYPES, addressTypes);
    }

    public void setFieldTypes(final ModelMap model) {
        final Map<String, String> fieldTypes = new HashMap<String, String>();
        for (FieldType fieldType: FieldType.values()) {
            fieldTypes.put(fieldType.name(), fieldType.name().toLowerCase());
        }

        model.addAttribute(FIELD_TYPES, fieldTypes);
    }

    public String getAvatar(final User user) throws IOException {
        byte[] avatar;
        if (user != null && user.getAvatar() != null) {
            avatar = user.getAvatar();
        } else {
            final InputStream inputStream = UserHelper.class.getResourceAsStream("/images/empty_avatar.jpg");
            if (inputStream == null) {
                return null;
            }
            avatar = ByteStreams.toByteArray(inputStream);
        }
        return new String(Base64.encode(avatar));
    }

    public long getSelectedGroupId(final User user, final long currentGroupId) {
        long newGroupId = currentGroupId;
        if (user.getGroups() == null || user.getGroups().isEmpty()) {
            final Group group = new Group();
            group.setId(currentGroupId);
            user.setGroups(Arrays.asList(group));
        } else {
            boolean hasCurrentGroup = false;
            for (Group group: user.getGroups()) {
                if (currentGroupId == group.getId()) {
                    hasCurrentGroup = true;
                    break;
                }
            }
            if (!hasCurrentGroup) {
                newGroupId = user.getGroups().get(0).getId();
            }
        }

        return newGroupId;
    }

    /**
     * User address types.
     */
    private enum AddressType {
        HOME, WORK, OTHER
    }

}
