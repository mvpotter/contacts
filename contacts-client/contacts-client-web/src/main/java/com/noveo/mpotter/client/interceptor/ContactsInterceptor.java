/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 04.07.14
 * Time: 15:33
 */
package com.noveo.mpotter.client.interceptor;

import com.noveo.mpotter.client.controller.GroupsController;
import com.noveo.mpotter.client.exception.ObjectNotFoundException;
import com.noveo.mpotter.client.model.Group;
import com.noveo.mpotter.soap.client.GroupClient;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Contacts web client requests interceptor.
 */
@Service
public class ContactsInterceptor implements HandlerInterceptor {

    private static final int GROUPS_ON_PAGE = 100;
    private static final String GROUPS = "groups";

    private final GroupClient groupClient;


    @Inject
    public ContactsInterceptor(final GroupClient groupClient) {
        this.groupClient = groupClient;
    }

    @Override
    public boolean preHandle(final HttpServletRequest httpServletRequest,
                             final HttpServletResponse httpServletResponse, final Object o) throws Exception {
        return true;
    }

    @Override
    public void postHandle(final HttpServletRequest httpServletRequest,
                           final HttpServletResponse httpServletResponse,
                           final Object o, final ModelAndView modelAndView) throws Exception {
        if (modelAndView != null) {
            final ModelMap model = modelAndView.getModelMap();
            final List<Group> groups = groupClient.getGroupsList(0, GROUPS_ON_PAGE);
            model.addAttribute(GROUPS, groups);
            final Object groupIdObj = model.get(GroupsController.GROUP);
            long groupId = 0;
            if (groupIdObj != null && groupIdObj instanceof Long) {
                groupId = (Long) groupIdObj;
            }
            if (groupId != 0) {
                boolean found = false;
                for (Group group: groups) {
                    if (group.getId() == groupId) {
                        model.addAttribute(GroupsController.GROUP, group);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    throw new ObjectNotFoundException();
                }
            }
        }
    }

    @Override
    public void afterCompletion(final HttpServletRequest httpServletRequest,
                                final HttpServletResponse httpServletResponse,
                                final Object o, final Exception e) throws Exception {

    }

}
