/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.client.converter.field.soap.impl;

import com.noveo.mpotter.client.converter.field.soap.AbstractSoapFieldConverter;
import com.noveo.mpotter.client.model.field.Field;
import com.noveo.mpotter.client.model.field.FieldType;
import com.noveo.mpotter.webservice.user.field.NumberField;

/**
 * Class for converting model number field to SOAP API's one.
 */
public class SoapNumberFieldConverter extends AbstractSoapFieldConverter {

    public SoapNumberFieldConverter() {
        supportedFieldClazz = NumberField.class;
    }

    @Override
    protected Field convertField(final com.noveo.mpotter.webservice.user.field.Field source) {
        final Field numberField = new Field();
        final NumberField soapNumberField = (NumberField) source;

        numberField.setType(FieldType.NUMBER);
        numberField.setName(soapNumberField.getName());
        numberField.setValue(String.valueOf(soapNumberField.getNumber()));

        return numberField;
    }
}
