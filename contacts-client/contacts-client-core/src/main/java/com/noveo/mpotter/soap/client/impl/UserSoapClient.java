/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 27.01.14
 * Time: 17:03
 */
package com.noveo.mpotter.soap.client.impl;

import com.noveo.mpotter.webservice.user.User;
import com.noveo.mpotter.webservice.user.UserClient;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Controller;

import javax.inject.Inject;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;
import javax.xml.ws.soap.SOAPFaultException;
import java.util.List;

/**
 * User SOAP client.
 */
@Controller
public class UserSoapClient extends AbstractSoapClient
                            implements com.noveo.mpotter.soap.client.UserClient {

    public static final String NAMESPACE_URI = "http://mpotter.noveo.com/webservice/user";

    private final UserClient userClient;

    @Inject
    public UserSoapClient(final ConversionService conversionService) {
        super(conversionService);
        final QName portName = new QName(NAMESPACE_URI, "UserClientPort");
        final QName serviceName = new QName(NAMESPACE_URI, "UserClientService");
        final Service service = Service.create(serviceName);
        service.addPort(portName, SOAPBinding.SOAP11HTTP_BINDING,
                "http://localhost:8080/contacts-soap-server/soap/user");
        userClient = service.getPort(portName, UserClient.class);
    }

    @Override
    public long saveUser(final com.noveo.mpotter.client.model.User user) {
        final User soapUser = conversionService.convert(user, User.class);
        return userClient.save(soapUser);
    }

    @Override
    public List<com.noveo.mpotter.client.model.User> getUsersList(final int page, final int itemsOnPage) {
        // TODO: implement
        // return userClient.getUsersList(page);
        return null;
    }

    @Override
    public com.noveo.mpotter.client.model.User getUser(final Long id) {
        final User soapUser = userClient.getUser(id);
        com.noveo.mpotter.client.model.User user;
        try {
            user = conversionService.convert(soapUser, com.noveo.mpotter.client.model.User.class);
        } catch (SOAPFaultException fault) {
            // TODO: show dialog with fault.getMessage()
            return null;
        }
        return user;
    }

}
