/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.client.converter.field.soap.impl;

import com.noveo.mpotter.client.converter.field.soap.AbstractSoapFieldConverter;
import com.noveo.mpotter.client.model.field.Field;
import com.noveo.mpotter.client.model.field.FieldType;
import com.noveo.mpotter.webservice.user.field.LinkField;

/**
 * Class for converting model link field to SOAP API's one.
 */
public class SoapLinkFieldConverter extends AbstractSoapFieldConverter {

    public SoapLinkFieldConverter() {
        supportedFieldClazz = LinkField.class;
    }

    @Override
    protected Field convertField(final com.noveo.mpotter.webservice.user.field.Field source) {
        final Field linkField = new Field();
        final LinkField soapLinkField = (LinkField) source;

        linkField.setType(FieldType.LINK);
        linkField.setName(soapLinkField.getName());
        linkField.setValue(soapLinkField.getValue());

        return linkField;
    }
}
