/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 05.08.13
 * Time: 18:20
 */
package com.noveo.mpotter.client.model.field;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Base field class.
 */
public class Field {

    public static final String DATE_FORMAT = "dd.MM.yyyy";

    protected FieldType type;
    @NotEmpty
    protected String name;
    @NotEmpty
    protected String value;

    public Field() {
    }

    public Field(final String name) {
        setName(name);
    }

    public FieldType getType() {
        return type;
    }

    public void setType(final FieldType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

}
