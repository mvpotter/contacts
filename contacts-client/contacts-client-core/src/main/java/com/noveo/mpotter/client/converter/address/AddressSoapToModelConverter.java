/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 22.04.14
 * Time: 11:28
 */
package com.noveo.mpotter.client.converter.address;


import com.noveo.mpotter.client.model.Address;
import org.springframework.core.convert.converter.Converter;

/**
 * Converts address SOAP entity to GUI model's one.
 */
public class AddressSoapToModelConverter implements Converter<com.noveo.mpotter.webservice.address.Address, Address> {

    @Override
    public Address convert(final com.noveo.mpotter.webservice.address.Address source) {
        final Address address = new Address();
        address.setType(source.getType());
        address.setCity(source.getCity());
        address.setStreet(source.getStreet());
        return address;
    }

}
