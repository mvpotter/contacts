/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 07.07.14
 * Time: 14:24
 */
package com.noveo.mpotter.client.model.field;

/**
 * Types of agent fields.
 */
public enum FieldType {
    DATE,
    NUMBER,
    EMAIL,
    LINK,
    TEXT
}
