/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 07.07.14
 * Time: 10:04
 */
package com.noveo.mpotter.client.converter.field.model;

import com.noveo.mpotter.client.model.field.Field;
import com.noveo.mpotter.client.model.field.FieldType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;

/**
 * Abstract field converter to implement a chain of converters for each field type.
 */
public abstract class AbstractFieldConverter implements FieldConverter {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractFieldConverter.class);

    protected FieldType supportedFieldType;
    protected FieldConverter nextConverter;

    public void setNext(final FieldConverter converter) {
        this.nextConverter = converter;
    }

    @Override
    public com.noveo.mpotter.webservice.user.field.Field convert(final Field source) {
        if (source.getType() == supportedFieldType) {
            try {
                return convertField(source);
            } catch (ParseException e) {
                LOGGER.warn(e.getMessage(), e);
            }
        }
        if (nextConverter != null) {
            return nextConverter.convert(source);
        }
        return null;
    }

    protected abstract com.noveo.mpotter.webservice.user.field.Field convertField(Field source) throws ParseException;
}
