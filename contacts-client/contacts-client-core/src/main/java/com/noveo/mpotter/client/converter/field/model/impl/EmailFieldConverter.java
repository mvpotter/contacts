/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.client.converter.field.model.impl;

import com.noveo.mpotter.client.converter.field.model.AbstractFieldConverter;
import com.noveo.mpotter.client.model.field.Field;
import com.noveo.mpotter.client.model.field.FieldType;

/**
 * Class for converting model email field to SOAP API's one.
 */
public class EmailFieldConverter extends AbstractFieldConverter {

    public EmailFieldConverter() {
        supportedFieldType = FieldType.EMAIL;
    }

    @Override
    protected com.noveo.mpotter.webservice.user.field.Field convertField(final Field source) {
        final com.noveo.mpotter.webservice.user.field.EmailField soapEmailField =
                new com.noveo.mpotter.webservice.user.field.EmailField();

        soapEmailField.setName(source.getName());
        soapEmailField.setValue(source.getValue());

        return soapEmailField;
    }
}
