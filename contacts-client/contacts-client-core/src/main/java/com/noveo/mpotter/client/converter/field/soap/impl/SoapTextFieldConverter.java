/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.client.converter.field.soap.impl;

import com.noveo.mpotter.client.converter.field.soap.AbstractSoapFieldConverter;
import com.noveo.mpotter.client.model.field.Field;
import com.noveo.mpotter.client.model.field.FieldType;
import com.noveo.mpotter.webservice.user.field.TextField;

/**
 * Class for converting model text field to SOAP API's one.
 */
public class SoapTextFieldConverter extends AbstractSoapFieldConverter {

    public SoapTextFieldConverter() {
        supportedFieldClazz = TextField.class;
    }

    @Override
    protected Field convertField(final com.noveo.mpotter.webservice.user.field.Field source) {
        final Field textField = new Field();
        final TextField soapTextField = (TextField) source;

        textField.setType(FieldType.TEXT);
        textField.setName(soapTextField.getName());
        textField.setValue(soapTextField.getValue());

        return textField;
    }
}
