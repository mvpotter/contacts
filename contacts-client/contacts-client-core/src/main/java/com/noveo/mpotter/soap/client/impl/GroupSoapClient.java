/*
* Created with IntelliJ IDEA.
* User: mpotter
* Date: 27.01.14
* Time: 16:57
*/
package com.noveo.mpotter.soap.client.impl;

import com.noveo.mpotter.soap.client.GroupClient;
import com.noveo.mpotter.webservice.common.Page;
import com.noveo.mpotter.webservice.group.Group;
import com.noveo.mpotter.webservice.group.GroupService;
import com.noveo.mpotter.webservice.group.GroupsList;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Controller;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;

/**
 * Group SOAP client.
 */
@Controller
public class GroupSoapClient extends com.noveo.mpotter.soap.client.impl.AbstractSoapClient implements GroupClient {

    private static final com.noveo.mpotter.webservice.group.GroupClient GROUP_CLIENT =
            new GroupService().getGroupClient();

    @Inject
    public GroupSoapClient(final ConversionService conversionService) {
        super(conversionService);
    }

    @Override
    public long saveGroup(final com.noveo.mpotter.client.model.Group group) {
        final Group soapGroup = conversionService.convert(group, Group.class);
        return GROUP_CLIENT.save(soapGroup);
    }

    @Override
    public List<com.noveo.mpotter.client.model.Group> getGroupsList(final int page, final int itemsOnPage) {
        final Page pageItem = new Page();
        pageItem.setPage(page);
        pageItem.setItemsOnPage(itemsOnPage);
        final List<com.noveo.mpotter.client.model.Group> groups =
                new LinkedList<com.noveo.mpotter.client.model.Group>();
        final GroupsList groupsList = GROUP_CLIENT.getGroupsList(pageItem);
        if (groupsList != null && groupsList.getGroups() != null) {
            for (Group group : groupsList.getGroups()) {
                groups.add(conversionService.convert(group, com.noveo.mpotter.client.model.Group.class));
            }
        }
        return groups;
    }

}
