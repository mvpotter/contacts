/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.client.converter.field.model.impl;

import com.noveo.mpotter.client.converter.field.model.AbstractFieldConverter;
import com.noveo.mpotter.client.model.field.Field;
import com.noveo.mpotter.client.model.field.FieldType;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Class for converting model date field to SOAP API's one.
 */
public class DateFieldConverter extends AbstractFieldConverter {

    public DateFieldConverter() {
        supportedFieldType = FieldType.DATE;
    }

    @Override
    protected com.noveo.mpotter.webservice.user.field.Field convertField(final Field source) throws ParseException {
        final com.noveo.mpotter.webservice.user.field.DateField soapDateField =
                new com.noveo.mpotter.webservice.user.field.DateField();

        soapDateField.setName(source.getName());
        soapDateField.setDate(new SimpleDateFormat(Field.DATE_FORMAT).parse(source.getValue()));

        return soapDateField;
    }
}
