/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 19/03/14
 * Time: 19:15
 */
package com.noveo.mpotter.client.converter.user;

import com.noveo.mpotter.client.converter.address.AddressArraySoapToModelConverter;
import com.noveo.mpotter.client.converter.field.soap.SoapFieldConverter;
import com.noveo.mpotter.client.converter.field.soap.SoapFieldConverterChainFactory;
import com.noveo.mpotter.client.converter.group.GroupSoapToModelConverter;
import com.noveo.mpotter.client.model.Group;
import com.noveo.mpotter.client.model.User;
import com.noveo.mpotter.client.model.field.Field;
import com.noveo.mpotter.webservice.address.Address;
import org.springframework.core.convert.converter.Converter;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Converts user SOAP entity to GUI model's one.
 */
public class UserSoapToModelConverter implements Converter<com.noveo.mpotter.webservice.user.User, User> {

    private static final GroupSoapToModelConverter GROUP_CONVERTER =
            new GroupSoapToModelConverter();
    private static final AddressArraySoapToModelConverter ADDRESS_ARRAY_CONVERTER =
            new AddressArraySoapToModelConverter();
    private static final SoapFieldConverter FIELD_CONVERTER = SoapFieldConverterChainFactory.createChain();

    @Override
    public User convert(final com.noveo.mpotter.webservice.user.User source) {
        final User user = new User();
        user.setId(source.getId());
        user.setLogin(source.getLogin());
        user.setAvatar(source.getAvatar());
        setGroups(user, source.getGroups());
        setAddresses(user, source.getAddresses());
        setFields(user, source.getFields());
        return user;
    }

    private void setGroups(final User user, final com.noveo.mpotter.webservice.group.Group[] soapGroups) {
        if (soapGroups != null) {
            final Group[] groups = new Group[soapGroups.length];
            for (int i = 0; i < groups.length; i++) {
                groups[i] = GROUP_CONVERTER.convert(soapGroups[i]);
            }
            user.setGroups(Arrays.asList(groups));
        }
    }

    private void setAddresses(final User user, final Address[] soapAddresses) {
        if (soapAddresses != null) {
            final com.noveo.mpotter.client.model.Address[] addresses =
                    ADDRESS_ARRAY_CONVERTER.convert(soapAddresses);
            user.setAddresses(Arrays.asList(addresses));
        }
    }

    private void setFields(final User user, final com.noveo.mpotter.webservice.user.field.Field[] soapFields) {
        if (soapFields != null) {
            final List<Field> fields = new LinkedList<Field>();

            for (com.noveo.mpotter.webservice.user.field.Field soapField: soapFields) {
                final Field field = FIELD_CONVERTER.convert(soapField);
                if (field != null) {
                    fields.add(field);
                }
            }

            user.setFields(fields);
        }
    }

}
