/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 22.04.14
 * Time: 11:33
 */
package com.noveo.mpotter.client.converter.address;


import com.noveo.mpotter.client.model.Address;
import org.springframework.core.convert.converter.Converter;

/**
 * Class for converting SOAP address array to an model's one.
 */
public class AddressArraySoapToModelConverter implements Converter<com.noveo.mpotter.webservice.address.Address[],
                                                                   Address[]> {

    private static final AddressSoapToModelConverter ADDRESS_CONVERTER = new AddressSoapToModelConverter();

    @Override
    public Address[] convert(final com.noveo.mpotter.webservice.address.Address[] source) {
        final Address[] addresses = new Address[source.length];
        for (int i = 0; i < source.length; i++) {
            addresses[i] = ADDRESS_CONVERTER.convert(source[i]);
        }
        return addresses;
    }
}
