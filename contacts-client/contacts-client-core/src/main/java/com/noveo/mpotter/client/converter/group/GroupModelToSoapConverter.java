/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 19/03/14
 * Time: 19:15
 */
package com.noveo.mpotter.client.converter.group;

import com.noveo.mpotter.client.model.Group;
import com.noveo.mpotter.client.model.User;
import org.springframework.core.convert.converter.Converter;

/**
 * Converts group GUI model's entity to SOAP one.
 */
public class GroupModelToSoapConverter implements Converter<Group, com.noveo.mpotter.webservice.group.Group> {

    private static final GroupUserModelToSoapConverter GROUP_USER_CONVERTER = new GroupUserModelToSoapConverter();

    @Override
    public com.noveo.mpotter.webservice.group.Group convert(final Group source) {
        final com.noveo.mpotter.webservice.group.Group soapGroup = new com.noveo.mpotter.webservice.group.Group();
        if (source.getId() != null) {
            soapGroup.setId(source.getId());
        }
        soapGroup.setName(source.getName());
        if (source.getUsers() != null) {
            for (User user: source.getUsers()) {
                soapGroup.getUsers().add(GROUP_USER_CONVERTER.convert(user));
            }
        }
        return soapGroup;
    }

}
