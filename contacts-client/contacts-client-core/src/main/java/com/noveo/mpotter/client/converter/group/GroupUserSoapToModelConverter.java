/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 19/03/14
 * Time: 19:15
 */
package com.noveo.mpotter.client.converter.group;

import com.noveo.mpotter.client.model.User;
import com.noveo.mpotter.webservice.group.GroupUser;
import org.springframework.core.convert.converter.Converter;

/**
 * Converts group user SOAP entity to GUI model's one.
 */
public class GroupUserSoapToModelConverter implements Converter<GroupUser, User> {

    @Override
    public User convert(final GroupUser source) {
        final User user = new User();
        user.setId(source.getId());
        user.setLogin(source.getName());
        return user;
    }

}
