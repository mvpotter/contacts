/*
* Created with IntelliJ IDEA.
* User: mpotter
* Date: 06.03.14
* Time: 17:25
*/
package com.noveo.mpotter.soap.client;


import com.noveo.mpotter.client.model.Group;

import java.util.List;

/**
 * Group client interface.
 */
public interface GroupClient {

    long saveGroup(Group group);
    List<Group> getGroupsList(int page, int itemsOnPage);

}
