/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 19/03/14
 * Time: 19:01
 */
package com.noveo.mpotter.client.model;

import javax.validation.constraints.Size;
import java.util.Set;

/**
 * GUI model Group entity.
 */
public class Group {

    private static final int NAME_MIN_LENGTH = 2;
    private static final int NAME_MAX_LENGTH = 30;

    protected Long id;
    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH)
    protected String name;
    protected Set<User> users;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(final Set<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Group group = (Group) o;

        if (id != null && id.equals(group.id)) {
            return true;
        }
        if (id != null ? !id.equals(group.id) : group.id != null) {
            return false;
        }
        if (name != null ? !name.equals(group.name) : group.name != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        final int magic = 31;
        if (id != null) {
            return id.hashCode();
        }
        int result = 0;
        result = magic * result + (name != null ? name.hashCode() : 0);
        return result;
    }

}
