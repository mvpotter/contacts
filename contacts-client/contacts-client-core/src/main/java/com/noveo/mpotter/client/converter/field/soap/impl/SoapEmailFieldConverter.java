/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.client.converter.field.soap.impl;

import com.noveo.mpotter.client.converter.field.soap.AbstractSoapFieldConverter;
import com.noveo.mpotter.client.model.field.Field;
import com.noveo.mpotter.client.model.field.FieldType;
import com.noveo.mpotter.webservice.user.field.EmailField;

/**
 * Class for converting model email field to SOAP API's one.
 */
public class SoapEmailFieldConverter extends AbstractSoapFieldConverter {

    public SoapEmailFieldConverter() {
        supportedFieldClazz = EmailField.class;
    }

    @Override
    protected Field convertField(final com.noveo.mpotter.webservice.user.field.Field source) {
        final Field emailField = new Field();
        final EmailField soapEmailField = (EmailField) source;

        emailField.setType(FieldType.EMAIL);
        emailField.setName(soapEmailField.getName());
        emailField.setValue(soapEmailField.getValue());

        return emailField;
    }
}
