/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 24.03.14
 * Time: 11:03
 */
package com.noveo.mpotter.soap.client.impl;

import org.springframework.core.convert.ConversionService;

/**
 * Abstract SOAP client with conversion service.
 */
public abstract class AbstractSoapClient {

    protected final ConversionService conversionService;

    public AbstractSoapClient(final ConversionService conversionService) {
        this.conversionService = conversionService;
    }

}
