/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 19/03/14
 * Time: 19:15
 */
package com.noveo.mpotter.client.converter.group;

import com.noveo.mpotter.client.model.Group;
import com.noveo.mpotter.client.model.User;
import com.noveo.mpotter.webservice.group.GroupUser;
import org.springframework.core.convert.converter.Converter;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Converts group GUI model's entity to SOAP one.
 */
public class GroupSoapToModelConverter implements Converter<com.noveo.mpotter.webservice.group.Group, Group> {

    private static final GroupUserSoapToModelConverter GROUP_USER_CONVERTER = new GroupUserSoapToModelConverter();

    @Override
    public Group convert(final com.noveo.mpotter.webservice.group.Group source) {
        final Group group = new Group();
        group.setId(source.getId());
        group.setName(source.getName());
        final Set<User> users = new LinkedHashSet<User>();
        for (GroupUser groupUser: source.getUsers()) {
            users.add(GROUP_USER_CONVERTER.convert(groupUser));
        }
        group.setUsers(users);
        return group;
    }

}
