/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 19/03/14
 * Time: 19:01
 */
package com.noveo.mpotter.client.model;

import com.noveo.mpotter.client.model.field.Field;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * GUI model user entity.
 */
public class User {

    private static final int LOGIN_MIN_LENGTH = 2;
    private static final int LOGIN_MAX_LENGTH = 30;

    private long id;
    @Size(min = LOGIN_MIN_LENGTH, max = LOGIN_MAX_LENGTH)
    private String login;
    private byte[] avatar;
    private List<Group> groups;
    @Valid
    private List<Address> addresses;
    @Valid
    private List<Field> fields;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(final byte[] avatar) {
        this.avatar = avatar;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(final List<Group> groups) {
        this.groups = groups;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(final List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(final List<Field> fields) {
        this.fields = fields;
    }

}
