/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 22.04.14
 * Time: 11:14
 */
package com.noveo.mpotter.client.model;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * User address.
 */
public class Address {

    protected long id;
    protected String type;
    @NotEmpty
    protected String city;
    @NotEmpty
    protected String street;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

}
