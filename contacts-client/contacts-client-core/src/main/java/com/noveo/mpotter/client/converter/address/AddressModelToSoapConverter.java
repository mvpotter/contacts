/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 22.04.14
 * Time: 11:28
 */
package com.noveo.mpotter.client.converter.address;


import com.noveo.mpotter.client.model.Address;
import org.springframework.core.convert.converter.Converter;

/**
 * Converts address model entity to SOAP API's one.
 */
public class AddressModelToSoapConverter implements Converter<Address, com.noveo.mpotter.webservice.address.Address> {

    @Override
    public com.noveo.mpotter.webservice.address.Address convert(final Address source) {
        final com.noveo.mpotter.webservice.address.Address address = new com.noveo.mpotter.webservice.address.Address();
        address.setType(source.getType());
        address.setCity(source.getCity());
        address.setStreet(source.getStreet());
        return address;
    }

}
