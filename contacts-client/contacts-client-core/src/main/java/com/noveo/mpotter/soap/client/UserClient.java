/*
* Created with IntelliJ IDEA.
* User: mpotter
* Date: 06.03.14
* Time: 17:25
*/
package com.noveo.mpotter.soap.client;


import com.noveo.mpotter.client.model.User;

import java.util.List;

/**
 * User client interface.
 */
public interface UserClient {

    long saveUser(User user);
    List<User> getUsersList(int page, int itemsOnPage);
    User getUser(Long id);

}
