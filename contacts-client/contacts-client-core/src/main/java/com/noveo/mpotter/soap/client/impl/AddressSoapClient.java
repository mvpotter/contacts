/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 27.01.14
 * Time: 16:57
 */
package com.noveo.mpotter.soap.client.impl;

import com.noveo.mpotter.webservice.address.Address;
import com.noveo.mpotter.webservice.address.AddressClient;
import com.noveo.mpotter.webservice.address.AddressService;
import com.noveo.mpotter.webservice.address.AddressesList;
import com.noveo.mpotter.webservice.common.Page;
import org.springframework.stereotype.Controller;

/**
 * Address SOAP client.
 */
@Controller
public class AddressSoapClient {

    private static final AddressClient ADDRESS_CLIENT = new AddressService().getAddressClient();

    public int addAddress(final Address address) {
        return ADDRESS_CLIENT.add(address);
    }

    public AddressesList getAddressesList(final Page page) {
        return ADDRESS_CLIENT.getAddressesList(page);
    }

}
