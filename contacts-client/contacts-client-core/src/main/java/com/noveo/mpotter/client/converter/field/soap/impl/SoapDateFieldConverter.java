/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.client.converter.field.soap.impl;

import com.noveo.mpotter.client.converter.field.soap.AbstractSoapFieldConverter;
import com.noveo.mpotter.client.model.field.Field;
import com.noveo.mpotter.client.model.field.FieldType;
import com.noveo.mpotter.webservice.user.field.DateField;

import java.text.SimpleDateFormat;

/**
 * Class for converting model date field to SOAP API's one.
 */
public class SoapDateFieldConverter extends AbstractSoapFieldConverter {

    public SoapDateFieldConverter() {
        supportedFieldClazz = DateField.class;
    }

    @Override
    protected Field convertField(final com.noveo.mpotter.webservice.user.field.Field source) {
        final Field dateField = new Field();
        final DateField soapDateField = (DateField) source;

        dateField.setType(FieldType.DATE);
        dateField.setName(soapDateField.getName());
        dateField.setValue(new SimpleDateFormat(Field.DATE_FORMAT).format(soapDateField.getDate()));

        return dateField;
    }
}
