/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 19/03/14
 * Time: 19:15
 */
package com.noveo.mpotter.client.converter.user;


import com.noveo.mpotter.client.converter.address.AddressArrayModelToSoapConverter;
import com.noveo.mpotter.client.converter.field.model.FieldConverter;
import com.noveo.mpotter.client.converter.field.model.FieldConverterChainFactory;
import com.noveo.mpotter.client.model.Address;
import com.noveo.mpotter.client.model.Group;
import com.noveo.mpotter.client.model.User;
import com.noveo.mpotter.client.model.field.Field;
import org.springframework.core.convert.converter.Converter;

import java.util.LinkedList;
import java.util.List;

/**
 * Converts user GUI model's entity to SOAP one.
 */
public class UserModelToSoapConverter implements Converter<User, com.noveo.mpotter.webservice.user.User> {

    private static final AddressArrayModelToSoapConverter ADDRESS_ARRAY_CONVERTER =
            new AddressArrayModelToSoapConverter();
    private static final FieldConverter FIELD_CONVERTER = FieldConverterChainFactory.createChain();

    @Override
    public com.noveo.mpotter.webservice.user.User convert(final User source) {
        final com.noveo.mpotter.webservice.user.User soapUser = new com.noveo.mpotter.webservice.user.User();
        soapUser.setId(source.getId());
        soapUser.setLogin(source.getLogin());
        soapUser.setAvatar(source.getAvatar());
        setGroups(soapUser, source.getGroups());
        setAddresses(soapUser, source.getAddresses());
        setFields(soapUser, source.getFields());
        return soapUser;
    }

    private void setGroups(final com.noveo.mpotter.webservice.user.User soapUser, final List<Group> groups) {
        if (groups != null) {
            final List<com.noveo.mpotter.webservice.group.Group> soapGroups =
                    new LinkedList<com.noveo.mpotter.webservice.group.Group>();
            for (Group group: groups) {
                final com.noveo.mpotter.webservice.group.Group soapGroup =
                        new com.noveo.mpotter.webservice.group.Group();
                soapGroup.setId(group.getId());
                soapGroup.setName(group.getName());
                soapGroups.add(soapGroup);
            }
            soapUser.setGroups(soapGroups.toArray(new com.noveo.mpotter.webservice.group.Group[0]));
        }
    }

    private void setAddresses(final com.noveo.mpotter.webservice.user.User soapUser,
                              final List<Address> addressesList) {
        if (addressesList != null) {
            final Address[] addresses = addressesList.toArray(new Address[0]);
            final com.noveo.mpotter.webservice.address.Address[] soapAddresses =
                    ADDRESS_ARRAY_CONVERTER.convert(addresses);
            soapUser.setAddresses(soapAddresses);
        }
    }

    private void setFields(final com.noveo.mpotter.webservice.user.User soapUser, final List<Field> fields) {
        if (fields != null) {
            final List<com.noveo.mpotter.webservice.user.field.Field> soapFields =
                    new LinkedList<com.noveo.mpotter.webservice.user.field.Field>();

            for (Field field: fields) {
                final com.noveo.mpotter.webservice.user.field.Field soapField =
                        FIELD_CONVERTER.convert(field);
                if (soapField != null) {
                    soapFields.add(soapField);
                }
            }

            final com.noveo.mpotter.webservice.user.field.Field[] soapFieldsArray =
                    soapFields.toArray(new com.noveo.mpotter.webservice.user.field.Field[0]);
            soapUser.setFields(soapFieldsArray);
        }
    }

}
