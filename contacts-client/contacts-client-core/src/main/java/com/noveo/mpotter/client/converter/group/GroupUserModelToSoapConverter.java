/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 19/03/14
 * Time: 19:15
 */
package com.noveo.mpotter.client.converter.group;

import com.noveo.mpotter.client.model.User;
import com.noveo.mpotter.webservice.group.GroupUser;
import org.springframework.core.convert.converter.Converter;

/**
 * Converts group user GUI model's entity to SOAP one.
 */
public class GroupUserModelToSoapConverter implements Converter<User, GroupUser> {

    @Override
    public GroupUser convert(final User source) {
        final GroupUser soapUser = new GroupUser();
        soapUser.setId(source.getId());
        soapUser.setName(source.getLogin());
        return soapUser;
    }

}
