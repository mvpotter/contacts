/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 22.04.14
 * Time: 11:33
 */
package com.noveo.mpotter.client.converter.address;


import com.noveo.mpotter.client.model.Address;
import org.springframework.core.convert.converter.Converter;

/**
 * Class for converting model address array to SOAP API's one.
 */
public class AddressArrayModelToSoapConverter
        implements Converter<Address[], com.noveo.mpotter.webservice.address.Address[]> {

    private static final AddressModelToSoapConverter ADDRESS_CONVERTER = new AddressModelToSoapConverter();

    @Override
    public com.noveo.mpotter.webservice.address.Address[] convert(final Address[] source) {
        final com.noveo.mpotter.webservice.address.Address[] addresses =
                new com.noveo.mpotter.webservice.address.Address[source.length];
        for (int i = 0; i < source.length; i++) {
            addresses[i] = ADDRESS_CONVERTER.convert(source[i]);
        }
        return addresses;
    }
}
