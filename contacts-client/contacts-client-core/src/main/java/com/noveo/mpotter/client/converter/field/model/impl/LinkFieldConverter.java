/*
 * Created with IntelliJ IDEA.
 * User: mpotter
 * Date: 17.01.14
 * Time: 17:32
 */
package com.noveo.mpotter.client.converter.field.model.impl;

import com.noveo.mpotter.client.converter.field.model.AbstractFieldConverter;
import com.noveo.mpotter.client.model.field.Field;
import com.noveo.mpotter.client.model.field.FieldType;

/**
 * Class for converting model link field to SOAP API's one.
 */
public class LinkFieldConverter extends AbstractFieldConverter {

    public LinkFieldConverter() {
        supportedFieldType = FieldType.LINK;
    }

    @Override
    protected com.noveo.mpotter.webservice.user.field.Field convertField(final Field source) {
        final com.noveo.mpotter.webservice.user.field.LinkField soapLinkField =
                new com.noveo.mpotter.webservice.user.field.LinkField();

        soapLinkField.setName(source.getName());
        soapLinkField.setValue(source.getValue());

        return soapLinkField;
    }
}
