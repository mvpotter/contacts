# Maven/JPA/JAX-WS training #

[Task description](https://bitbucket.org/mvpotter/contacts/src/afe9d9366742e2baeb1cc57b25ce3eaacc39bf29/Task.md?at=master)

Prerequisites
-------------
Install project modules to local maven repository

```
cd project_root
mvn install
```

Profiles
----------
* **check-source** - check source code with maven checkstyle and pmd plugins
* **build-source** - build jar with source code
* **build-docs** - build jar with javadocs
* **integration-test** - run integration tests
* **sign** - sign artifacts


Launch server
------------
```
cd contacts-soap/contacts-soap-server
mvn tomcat7:run
```

Launch client
-------------

```
cd contacts-client/contacts-client-web
mvn tomcat7:run
```

Default client launch address:

http://127.0.0.1:9080/contacts-client-web