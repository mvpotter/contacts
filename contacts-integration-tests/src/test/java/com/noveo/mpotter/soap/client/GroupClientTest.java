/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 17/05/14
 * Time: 19:16
 */
package com.noveo.mpotter.soap.client;

import com.noveo.mpotter.client.model.Group;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;
import javax.xml.ws.soap.SOAPFaultException;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:com/noveo/mpotter/contacts-client-core-context.xml",
                                   "classpath:com/noveo/mpotter/contacts-client-core-converters.xml"})
public class GroupClientTest {

    @Inject
    private GroupClient groupClient;

    @Before
    public void before() {
        Group group = new Group();
        group.setName("Test group");
        groupClient.saveGroup(group);
    }

    @Test
    public void testGetGroupsList() {
        List<Group> groups = groupClient.getGroupsList(0, 100);
        Assert.assertTrue(groups.size() > 0);
    }

    @Test (expected = SOAPFaultException.class)
    public void testSaveGroupWithEmptyFields() {
        Group group = new Group();
        groupClient.saveGroup(group);
    }

}
